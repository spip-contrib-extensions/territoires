<?php
/**
 * Ce fichier contient les fonctions de service nécessitées par l'utilisation du plugin `Territoires`.
 *
 * @package SPIP\TERRITOIRES\SERVICES
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_TERRITOIRES_URL_BASE_ISOCODE')) {
	/**
	 * Endpoint de l'API REST hébergeant les données de Nomenclatures.
	 */
	define('_TERRITOIRES_URL_BASE_ISOCODE', 'https://contrib.spip.net/http.api/ezrest');
}

if (!defined('_TERRITOIRES_COMPATIBILITE_ISOCODE')) {
	/**
	 * Endpoint de l'API REST hébergeant les données de Nomenclatures.
	 */
	define('_TERRITOIRES_COMPATIBILITE_ISOCODE', ['vmin' => '2.0.1']);
}

// ------------------------------------------------------
// ----------- SERVICES GENERAUX DU PLUGIN --------------
// ------------------------------------------------------

/**
 * Renvoie, pour le plugin appelant, les bornes de compatibilité avec le serveur fournissant une nature d'informations
 * donnée sur les territoires.
 * En fait, la nature d'information est liée au plugin appelant (nomenclatures pour Territoires, contours géographiques
 * pour le plugin Contours de territoires, etc.).
 *
 * @uses territoires_chercher_service()
 *
 * @param string $plugin Préfixe du plugin appelant.
 *
 * @return array Bornes de compatibilité avec le serveur.
 */
function territoires_serveur_definir_compatibilite(string $plugin) : array {
	// On passe la main à un service spécifique du plugin appelant si demandé.
	if ($definir = territoires_chercher_service($plugin, 'serveur_definir_compatibilite')) {
		$bornes = $definir();
	} else {
		// Renvoyer les bornes de compatibilité par défaut
		$bornes = _TERRITOIRES_COMPATIBILITE_ISOCODE;
	}

	return $bornes;
}

/**
 * Recharge la configuration du plugin.
 * Le service appelle un pipeline homonyme permettant de prolonger l'action au-delà du plugin utilisateur.
 *
 * Le plugin Territoires recharge sa propre configuration statique.
 *
 * @uses territoires_chercher_service()
 * @pipeline_appel configuration_recharger
 *
 * @param string $plugin Préfixe du plugin utilisateur
 *
 * @return void
 */
function territoires_configuration_recharger(string $plugin) : void {
	// Rechargement de la configuration statique.
	include_spip('territoires_administrations');
	// -- Configuration statique du plugin Territoires
	$config_statique = territoires_configurer('statique');
	// -- Configuration dynamique du plugin Territoires
	$config_modifiable = territoires_configurer('utilisateur');
	// -- Ecriture de la configuration : on passe les deux configurations pour assurer un rechargement correct
	territoires_adapter_config_statique($config_statique, $config_modifiable);

	// Si le plugin utilisateur veut compléter l'action par un post-traitment il doit utiliser le pipeline homonyme.
	$flux = [
		'args' => [
			'plugin' => $plugin,
		],
		'data' => []
	];
	pipeline('configuration_recharger', $flux);
}

// -----------------------------------------------------------------------
// ------------------- SERVICES RELATIFS AUX FEEDS -----------------------
// -----------------------------------------------------------------------

/**
 * Renvoie l'URL de base du serveur Nomenclatures fournissant des informations sur les territoires.
 *
 * @uses territoires_chercher_service()
 *
 * @param string $plugin Préfixe du plugin appelant.
 *
 * @return string URL de base du serveur REST
 */
function territoires_feed_initialiser_url_base(string $plugin) : string {
	// On passe la main à un service spécifique du plugin appelant si demandé.
	if ($initialiser = territoires_chercher_service($plugin, 'feed_initialiser_url_base')) {
		$url_base = $initialiser();
	} else {
		// Territoires utilise par défaut l'URL de Contrib
		$url_base = _TERRITOIRES_URL_BASE_ISOCODE;
	}

	return $url_base;
}

/**
 * Renvoie la liste des catégories de feed de Nomenclatures correspondant à la nature des informations demandées.
 * Pour Territoires, la nomenclature est fournie au travers de la catégorie de feed nommée `territory`.
 *
 * @uses territoires_chercher_service()
 *
 * @param string $plugin Préfixe du plugin appelant.
 *
 * @return array Liste des catégories de feeds de Nomenclatures utilisées par le plugin appelant.
 */
function territoires_feed_categorie_lister(string $plugin) : array {
	// On passe la main à un service spécifique du plugin appelant si demandé.
	if ($lister = territoires_chercher_service($plugin, 'feed_categorie_lister')) {
		$categories = $lister();
	} else {
		// Territoires récupère la nomenclature des territoires fournie par le plugin Nomenclatures
		// au travers des feeds de la catégorie territory
		$categories = ['territory'];
	}

	return $categories;
}

/**
 * Renvoie, pour la collection `feeds`, l'index du hash permettant de savoir si le ou les feeds récupérés sont
 * obsolètes.
 *
 * @uses territoires_chercher_service()
 *
 * @param string $plugin Préfixe du plugin appelant.
 * @param string $type   Type de territoires. Prends les valeurs `zone`, `country`, `subdivision`, `protected_area` ou `infrasubdivision`.
 *
 * @return string Index du hash dans la réponse à la requête REST.
 */
function territoires_feed_indexer_hash(string $plugin, string $type) : string {
	// On passe la main à un service spécifique du plugin appelant si demandé.
	if ($initialiser = territoires_chercher_service($plugin, 'feed_indexer_hash')) {
		$index_hash = $initialiser($type);
	} else {
		// Territoires utilise différents index suivant le type de territoires : inscrit dans la configuration
		include_spip('inc/config');
		$index_hash = lire_config("territoires/{$type}/index_hash", '');
	}

	return $index_hash;
}

/**
 * Liste les feeds fournissant des informations de territoires d'une nature donnée exprimée sous la forme
 * de catégories.
 *
 * @uses territoires_feed_initialiser_url_base()
 * @uses requeter_isocode()
 *
 * @param string $plugin          Préfixe du plugin appelant.
 * @param array  $categories_feed Liste des catégories de feeds à acquérir.
 *
 * @return array Liste des feeds.
 */
function territoires_feed_acquerir(string $plugin, array $categories_feed) : array {
	// Initialisation du tableau de sortie.
	static $feeds = [];

	// Construire les filtres permettant ensuite de calculer l'url de la requête
	$index = implode(',', $categories_feed);
	$filtres = [
		'categorie' => $index,
	];

	// Identifier l'url de base pour le plugin appelant
	$url_base = territoires_feed_initialiser_url_base($plugin);

	if (!isset($feeds[$index])) {
		// Appel à l'API REST de Nomenclatures
		$requeter = charger_fonction('requeter_isocode', 'inc');
		$reponse = $requeter($url_base, 'feeds', $filtres);
		if ((int) ($reponse['erreur']['status']) === 200) {
			$feeds[$index] = $reponse['donnees'];
		}
	}

	return $feeds[$index] ?? [];
}

// -----------------------------------------------------------------------
// ------------ SERVICES RELATIFS AUX UNITES DE PEUPLEMENT ---------------
// -----------------------------------------------------------------------

/**
 * Renvoie la collection de Nomenclatures à requêter en fonction de l’unité de peuplement concernée.
 *
 * @uses territoires_chercher_service()
 *
 * @param string      $plugin  Préfixe du plugin appelant.
 * @param string      $type    Type de territoires. Prends les valeurs `zone`, `country`, `subdivision`, `protected_area` ou `infrasubdivision`.
 * @param null|string $pays    Code ISO 3166-1 alpha2 du pays si le type est `subdivision` ou `infrasubdivision` sinon une chaine vide.
 * @param null|string $service Identifiant du feed Nomenclatures ayant permis le chargement. Est utilisé pour indexer
 *                             la consignation dans le cas des contours uniquement. Sinon vaut chaine vide.
 *
 * @return string Identifiant de la collection.
 */
function territoires_unite_peuplement_definir_collection(string $plugin, string $type, ?string $pays = '', ?string $service = '') : string {
	// On passe la main à un service spécifique du plugin appelant si demandé.
	if ($definir = territoires_chercher_service($plugin, 'unite_peuplement_definir_collection')) {
		$collection = $definir($type, $pays, $service);
	} else {
		// Pour Territoires, la collection diffère pour chaque type
		include_spip('inc/config');
		$collection = lire_config("territoires/{$type}/collection", '');
	}

	return $collection;
}

/**
 * Renvoie, les filtres à appliquer à la requête Nomenclatures en fonction de l’unité de peuplement voire du service concerné.
 *
 * @uses territoires_chercher_service()
 *
 * @param string      $plugin  Préfixe du plugin appelant.
 * @param string      $type    Type de territoires. Prends les valeurs `zone`, `country`, `subdivision`, `protected_area` ou `infrasubdivision`.
 * @param null|string $pays    Code ISO 3166-1 alpha2 du pays si le type est `subdivision` ou `infrasubdivision` sinon une chaine vide.
 * @param null|string $service Identifiant du feed Nomenclatures ayant permis le chargement. Est utilisé pour indexer
 *                             la consignation dans le cas des contours uniquement. Sinon vaut chaine vide.
 *
 * @return array Filtres à appliquer à la collection.
 */
function territoires_unite_peuplement_definir_filtre(string $plugin, string $type, ?string $pays = '', ?string $service = '') : array {
	// On passe la main à un service spécifique du plugin appelant si demandé.
	$filtres = [];
	if ($definir = territoires_chercher_service($plugin, 'unite_peuplement_definir_filtre')) {
		$filtres = $definir($type, $pays, $service);
	} else {
		// Pour Territoires, seul le filtre sur le pays peut être utilisé suivant la configuration du type.
		include_spip('inc/config');
		if (
			lire_config("territoires/{$type}/populated_by_country", false)
			and $pays
		) {
			$filtres = ['pays' => $pays];
		}
	}

	return $filtres;
}

// -----------------------------------------------------------------------
// ------------------------ SERVICES INTERNES ----------------------------
// -----------------------------------------------------------------------

/**
 * Cherche une fonction donnée en se basant sur le service de stockage ou à défaut sur le plugin appelant.
 * Si ni le service de stockage ni le plugin ne fournissent la fonction demandée la chaîne vide est renvoyée.
 *
 * @internal
 *
 * @param string     $plugin   Préfixe du plugin utilisateur.
 * @param string     $fonction Nom de la fonction à chercher.
 * @param null|array $options  Options de calcul de la fonction (non utilisé auourd'hui)
 *
 * @return string Nom complet de la fonction si trouvée ou chaine vide sinon.
 */
function territoires_chercher_service(string $plugin, string $fonction, ?array $options = []): string {
	$fonction_trouvee = '';

	// Eviter la réentrance si on demande le plugin Territoires
	if ($plugin !== 'territoires') {
		include_spip("territoires/{$plugin}");

		// On teste l'existence de la fonction dans le plugin appelant.
		$fonction_trouvee = "{$plugin}_{$fonction}";
		if (!function_exists($fonction_trouvee)) {
			$fonction_trouvee = '';
		}
	}

	return $fonction_trouvee;
}
