<?php
/**
 * Ce fichier contient les fonctions d'API du plugin Territoires utilisées comme filtre dans les squelettes.
 *
 * @package SPIP\TERRITOIRES\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Compile la balise `#TERRITOIRE_ASCENDANCE` qui renvoie l'ascendance d'un territoire donné
 * désigne par son identifiant primaire.
 * La signature de la balise est : `#TERRITOIRE_ASCENDANCE{iso_territoire[, parent, ordre]}`.
 *
 * @balise
 *
 * @uses territoire_lire_ascendance()
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_TERRITOIRE_ASCENDANCE_dist(Champ $p) : Champ {
	// Récupération des arguments.
	// -- la balise utilise toujours le rangement par rang au sein du conteneur
	// -- et ne permet pas de filtrer les noisettes autrement que sur le conteneur.
	$iso_territoire = interprete_argument_balise(1, $p);
	$iso_territoire = isset($iso_territoire) ? str_replace('\'', '"', $iso_territoire) : '""';
	$iso_parent = interprete_argument_balise(2, $p);
	$iso_parent = isset($iso_parent) ? str_replace('\'', '"', $iso_parent) : '""';
	$ordre = interprete_argument_balise(3, $p);
	$ordre = isset($ordre) ? str_replace('\'', '"', $ordre) : '"descendant"';

	// On appelle la fonction de calcul de la liste des noisettes
	$p->code = "(include_spip('inc/territoire')
		? territoire_lire_ascendance({$iso_territoire}, {$iso_parent}, {$ordre})
		: [])";

	return $p;
}

/**
 * Compile la balise `#TERRITOIRE_EXTRAS` qui renvoie tout ou partie des informations extras d'un territoire donné
 * désigne par son identifiant primaire.
 * La signature de la balise est : `#TERRITOIRE_EXTRAS{iso_territoire[, information]}`.
 *
 * @balise
 *
 * @uses territoire_lire_extras()
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_TERRITOIRE_EXTRAS_dist(Champ $p) : Champ {
	// Récupération des arguments.
	// -- la balise utilise toujours le rangement par rang au sein du conteneur
	// -- et ne permet pas de filtrer les noisettes autrement que sur le conteneur.
	$iso_territoire = interprete_argument_balise(1, $p);
	$iso_territoire = isset($iso_territoire) ? str_replace('\'', '"', $iso_territoire) : '""';
	$information = interprete_argument_balise(2, $p);
	$information = isset($information) ? str_replace('\'', '"', $information) : '""';

	// On appelle la fonction de calcul de la liste des noisettes
	$p->code = "(include_spip('inc/territoire')
		? territoire_lire_extras({$iso_territoire}, {$information})
		: [])";

	return $p;
}
