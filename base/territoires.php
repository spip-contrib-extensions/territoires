<?php
/**
 * Déclarations relatives à la base de données.
 *
 * @package SPIP\TERRITOIRES\INSTALLATION
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Déclaration des alias de tables et filtres automatiques de champs.
 *
 * @pipeline declarer_tables_interfaces
 *
 * @param array $interfaces Déclarations d'interface pour le compilateur
 *
 * @return array Déclarations d'interface mis à jour
 */
function territoires_declarer_tables_interfaces(array $interfaces) : array {
	$interfaces['table_des_tables']['territoires'] = 'territoires';
	$interfaces['table_des_tables']['territoires_extras'] = 'territoires_extras';

	$interfaces['table_des_traitements']['NOM_USAGE']['territoires'] = _TRAITEMENT_TYPO;
	$interfaces['table_des_traitements']['ISO_TITRE']['territoires'] = _TRAITEMENT_TYPO;
	$interfaces['table_des_traitements']['DESCRIPTIF']['territoires'] = _TRAITEMENT_RACCOURCIS;

	return $interfaces;
}

/**
 * Déclaration des objets éditoriaux.
 * Le plugin Territoires déclare 1 nouvelle table objet, `spip_territoires`.
 *
 * @pipeline declarer_tables_objets_sql
 *
 * @param array $tables Description des tables d'objet
 *
 * @return array Description complétée des tables d'objet
 */
function territoires_declarer_tables_objets_sql(array $tables) : array {
	$tables['spip_territoires'] = [
		'type'       => 'territoire',
		'principale' => 'oui',
		'field'      => [
			'id_territoire'   => 'bigint(21) NOT NULL',
			'iso_territoire'  => 'varchar(20) NOT NULL DEFAULT ""',
			'iso_titre'       => 'text NOT NULL DEFAULT ""',
			'nom_usage'       => 'text NOT NULL DEFAULT ""',
			'descriptif'      => 'longtext NOT NULL DEFAULT ""',
			'type'            => 'varchar(20) DEFAULT "" NOT NULL',
			'categorie'       => 'varchar(64) NOT NULL DEFAULT ""',
			'iso_continent'   => 'varchar(3) DEFAULT "" NOT NULL',
			'iso_pays'        => 'varchar(2) NOT NULL DEFAULT ""',
			'iso_parent'      => 'varchar(20) NOT NULL DEFAULT ""',
			'profondeur_type' => 'int NOT NULL DEFAULT 0',
			'iso_parent_alt'  => 'varchar(20) NOT NULL DEFAULT ""',
			'edite'           => 'varchar(3) NOT NULL DEFAULT "non"',
			'date'            => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			'maj'             => 'TIMESTAMP NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'
		],
		'key' => [
			'PRIMARY KEY'        => 'id_territoire',
			'KEY iso_territoire' => 'iso_territoire',
			'KEY iso_parent'     => 'iso_parent',
			'KEY iso_parent_alt' => 'iso_parent_alt',
			'KEY iso_pays'       => 'iso_pays',
			'KEY iso_continent'  => 'iso_continent',
		],
		'titre'             => 'nom_usage AS titre, "" AS lang',
		'date'              => 'date',
		'champs_editables'  => ['descriptif'],
		'champs_versionnes' => ['descriptif'],
		'rechercher_champs' => ['iso_territoire' => 10, 'iso_titre' => 5, 'nom_usage' => 5, 'descriptif' => 2],
		'tables_jointures'  => ['spip_territoires_liens'],

		'texte_retour'       => 'icone_retour',
		'texte_objets'       => 'territoire:titre_territoires',
		'texte_objet'        => 'territoire:titre_territoire',
		'texte_modifier'     => 'territoire:icone_modifier_territoire',
		'texte_creer'        => 'territoire:icone_creer_territoire',
		'info_aucun_objet'   => 'territoire:info_aucun_territoire',
		'info_1_objet'       => 'territoire:info_1_territoire',
		'info_nb_objets'     => 'territoire:info_nb_territoires',
		'texte_logo_objet'   => 'territoire:titre_logo_territoire',
		'texte_langue_objet' => 'territoire:titre_langue_territoire',

		// Par défaut, Territoires prévoit une gestion des rôles mais n'en fournit pas
		'roles_colonne' => 'role',
		'roles_titres'  => [],
		'roles_objets'  => [],
	];

	return $tables;
}

/**
 * Déclaration des tables secondaires (liaisons).
 * Le plugin Territoires déclare 2 nouvelle tables auxilliaire:
 * - celle des liens, `spip_territoires_liens`
 * - celle des caractéristiques complémentaires, `spip_territoires_extras`.
 *
 * @pipeline declarer_tables_auxiliaires
 *
 * @param array $tables Description des tables auxilliaires
 *
 * @return array Description complétée des tables auxilliaires
 */
function territoires_declarer_tables_auxiliaires(array $tables) : array {
	// Table d'extensions des données d'un territoire :
	// -- contient les codes alternatifs, les caractéristiques géographiques (superficie, population...) ou autres.
	$tables['spip_territoires_extras'] = [
		'field' => [
			'iso_territoire' => 'varchar(20) NOT NULL DEFAULT ""',  // identifiant primaire du territoire
			'extra'          => 'varchar(255) NOT NULL DEFAULT ""', // type d'attribut (unique pour un territoire)
			'valeur'         => 'text DEFAULT ""',                  // valeur quelconque comme une meta
			'type_extra'     => 'varchar(4) DEFAULT "" NOT NULL',   // type du champ extra (code ou info)
			'type'           => 'varchar(20) DEFAULT "" NOT NULL',  // recopie du champ de spip_territoires (optimisation)
			'iso_pays'       => 'varchar(2) DEFAULT "" NOT NULL',   // recopie du champ de spip_territoires (optimisation)
			'maj'            => 'timestamp NOT NULL DEFAULT current_timestamp ON UPDATE current_timestamp'
		],
		'key' => [
			'PRIMARY KEY'    => 'iso_territoire,extra',
			'KEY extra'      => 'extra',
			'KEY type_extra' => 'type_extra',
			'KEY type'       => 'type',
			'KEY iso_pays'   => 'iso_pays',
		]
	];

	// Table de liens avec les autres objets SPIP
	$tables['spip_territoires_liens'] = [
		'field' => [
			'id_territoire' => 'bigint(21) DEFAULT "0" NOT NULL',
			'objet'         => 'varchar(25) DEFAULT "" NOT NULL',
			'id_objet'      => 'bigint(21) DEFAULT "0" NOT NULL',
			'role'          => 'varchar(30) NOT NULL DEFAULT ""',
			'vu'            => 'varchar(6) DEFAULT "non" NOT NULL',
		],
		'key' => [
			'PRIMARY KEY'       => 'id_territoire,id_objet,objet,role',
			'KEY id_territoire' => 'id_territoire',
			'KEY objet'         => 'objet',
			'KEY id_objet'      => 'id_objet',
			'KEY role'          => 'role'
		]
	];

	return $tables;
}
