# Changelog

## [Unreleased]

### Added

- Inserer la liste des Cartes de Territoires dans le menu de navigation global
- Ajout d'un service de rechargement de la configuration prolongé par un pipeline pour les autres plugin de l'écosystème

## [2.0.1]

### Changed

- Changement des icones de type de territoires (erational)

### Fixed

- Mise à jour du guide pour prendre en compte tous les changements majeurs depuis 1.5.0
- Correction de la fonction de vidage des territoires pour ne supprimer que les codes alternatifs associés

## [2.0.0]

### Changed

- Prise en compte du changement de préfixe du plugin Jeux de données pour Territoires
- Mise à jour des consignations suite au transfert des extras
- Mise au point du readme
- Mise au point des fichiers de langue suite à des transferts entre plugins de territoires

### Removed

- Transfert de la gestion et de l'affichage des extras (hors codes) dans le plugin Jeux de données pour Territoires

### Added

- Ajout du code postal des communes dans la liste des codes alternatifs

## [1.5.7]

### Fixed

- Prise en compte d'un changement de fonction dans l'API de Saisies

## [1.5.6]

### Fixed

- Correction de la gestion des caches
- Mise au point des items de langue

### Added

- Mise en place d'un menu pour les données statistiques

## [1.5.5]

### Changed

- Nouveau logo par erational
- Mise au point des traductions en français et en anglais

### Fixed

- Mise à jour du guide
- Correction du calcul de la consigne

## [1.5.4]

### Added

- Ajout d'un bouton de rechargement de la configuration statique
- Rendre disponible la gestion des rôles pour typer le lien d'un objet quelconque avec un territoire
- Ajout d'une option extraire_multi pour les saisies checkbox et radio en mode flex.

## [1.5.3] - 2023-05-28

Cette version est un refactoring significatif du code du plugin. De fait, il peut être
nécessaire de recharger certaines unités de peuplement de territoires.

### Added

- Avertir l'utilisateur dans le formulaire de peuplement si le serveur des nomenclatures de territoires n'est pas compatible.
- Rangement du formulaire de peuplement par onglets.
- Ajout d'une information d'obsolescence d'une unité de peuplement dans le formulaire de peuplement.
- Ajout d'un type de territoire `zone protégée` pour les parcs et réserves naturels qui ne sont
donc plus considérés comme des subdivisions.
- Création de fonctions de service pour des plugins connexes comme `Contours de territoires`.
- Ajout d'un changelog.

### Changed

- Refactorer l'affichage du formulaire de peuplement
- Refactorer le chargement du formulaire de peuplement en utilisant la collection des feeds
(performance).
- Refactoring complet de l'organisation des fonctions de peuplement.

## [1.5.1] - 2023-05-04

### Changed

- Formatage des données extra d'un territoire avec affichage de l'unité si nécessaire.

## [1.5.0] - 2023-04-30

### Added

- Ajout de la nomenclature des parcs naturels considérés comme des subdivisions.
- Compatibilité spip 4.2.

### Changed

- Faire évoluer la fonction de fusion d'une traduction pour prendre en compte spip 4.2 et son collecteur Multis.
- Mise à jour d'items de langue

### Fixed

- Corriger l'appel à territoires_acquerir() dont le paramèttre pays est une chaine pas un tableau.
- Corriger la fonction de fusion des traductions

## [1.4.3] - 2022-12-22

### Changed

- Mise à jour de certaines écritures PHP pour s'adapter à PHP 8.2
- Actions qualité sur le code : nettoyage, amélioration, typage des fonctions, PHPDoc systématique.
