# Plugin Territoires

## Introduction

L’objectif du plugin est de fournir un nouvel objet éditorial couvrant l’ensemble des zones géographiques du continent à
la subdivision minimale d’un pays.

Le plugin fournit, les élément standard d’un objet éditorial « territoire », comme son interface privée (édition,
liste, configuration) et une interface de peuplement des données à partir de l’API REST du plugin Nomenclatures. Il
propose également un mécanisme d’extension des données attachées à chaque territoire.

## Documentation

La documentation utilisateur du plugin est disponible sur [SPIP-Contrib](https://contrib.spip.net/Territoires-utilisation-du-plugin).
Pour plus d'informations sur la conception du plugin, il est possible de consulter le guide PDF inclu dans le code
du plugin (`Guide - Le plugin Territoires.pdf`).

## Installation

Le plugin s’installe comme n’importe quel plugin SPIP. Il nécessite uniquement les plugins Saisies pour formulaires et
Cache Factory.

## Les types de territoire

De façon à retrouver quelque peu la logique actuelle des objets géographiques (pays, continents, subdivisions
françaises) et de coller à la standardisation internationale, les objets « territoire » sont classés en plusieurs
groupes ou types, à savoir :

- Les **zones**, qui délimitent des zones supranationales par des regroupements de pays en continents, sous-continents et
  autres régions conformément à la norme UN M49. Le type est `zone`.
- les **pays**, que l’on peut considérer comme le type pivot des territoires et dont la liste est issue de la norme ISO
  3166-1. Le type associé aux pays est `country` ;
- les **subdivisions** d’un pays, qui en organisent, conformément à la norme ISO 3166-2, la structure administrative à
  l’instar des länder en Allemagne ou des régions et départements en France. Le type associé est `subdivision`.
- les **infra-subdivisions** d’un pays, qui raffinent les subdivisions standard mais ne font pas partie de la norme ISO
  3166-2. Elles possèdent une identification spécifique qui varie d’un pays à un autre. C’est le cas, par exemple, des
  arrondissements, cantons et communes de France dont l’identifiant est le code INSEE ou des comtés américains
  identifiés par le code FIPS. Le type associé est `infrasubdivision`.
- et depuis la version 1.5.0, les **zones protégées**, qui regroupent les parcs et réserves naturelles terrestres ou marines
  de tout genre. Elles possèdent une identification internationale fournit par l’organisation Protected Planet au
  travers de la base de données mondiale nommée WDPA. Le type associé est `protected_area`.

L'ensemble de ces territoires forme une **hiérarchie** du monde à la division minimale d'un pays.

## Liens & rôles

Territoires permet à tout objet autorisé de se lier avec un ou plusieurs territoires.
En outre, Territoires est compatible avec la gestion des rôles, ce qui veut dire qu'il est possible de typer les
liens vers un territoire.

Par exemple, le plugin Taxonomie peut lier une espèce à plusieurs territoires, certains pouvant désigner les
territoires d'orgine de l'espèce, d'autres ceux où l'espèce a été introduite ou réintroduite, etc.

## Peuplement et édition

Les territoires sont créés automatiquement au travers d'un formulaire dédié dans l'espace privé. Les territoires
sont chargés par unité de peuplement depuis un serveur REST motorisé par Nomenclatures. On charge donc d'un coup les
pays, les régions du monde, les lander allemands, etc.
A l'installation du plugin, les territoires suivants sont chargés:

- les continents et régions du monde,
- les pays,
- les régions et départements français,
- ainsi que les arrondissements, communes et EPCI françaises.

Il n'est pas possible de supprimer unitairement un territoire, la suppression se fait aussi par unité de peuplement.

Par contre, il est possible d'éditer un territoire et de modifier sa description ou d'ajouter un logo. En outre, lors du peuplement
des territoires, les codes d'identification alternatifs comme les codes NUTS, INSEE ou autres sont également chargés.

## Mise à jour des données

Si les données d'une unité de peuplement sont modifiées sur le serveur Nomenclatures, le formulaire de chargement de
Territoires l'indique par un libellé explicite. L'administrateur peut alors recharger les données de l'unité de
peuplement concernée sans perdre les éventuelles modifications effectuées sur les territoires rechargés ni les liens
éventuels qui sont sauvegardés et restitués.

## Plugins connexes

Le plugin Territoires sert aussi de base au plugin Contours des territoires qui propose d’ajouter des contours
GeoJSON aux territoires. Le plugin Territoires accueille ainsi le formulaire de chargement de contours dans sa page de
peuplement, le contour lui-même étant affiché dans la fiche objet du territoire comme un objet GIS non modifiable. Il aussi
est complété par le plugin Jeux de données pour Territoires qui fournit des données complémentaires sur les territoires.
