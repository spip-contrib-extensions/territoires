<?php
/**
 * Ce fichier contient l'action `recharger_config_territoires` lancée par un utilisateur pour
 * recharger, de façon sécurisée, la configuration statique de Territoires.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet à l'utilisateur de recharger en meta, de façon sécurisée,
 * la configuration statique du plugin.
 *
 * Cette action est réservée aux utilisateurs pouvant configurer le plugin
 * Elle ne nécessite aucun argument.
 *
 * @uses feed_charger()
 *
 * @return void
 */
function action_recharger_config_territoires_dist() : void {
	// Verification des autorisations : pour recharger les types de contrôle il suffit
	// d'avoir l'autorisation de configurer le plugin.
	if (!autoriser('configurer', '_territoires')) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// Rechargement de la configuration statique du plugin et propagation éventuelle vers d'autres plugins
	include_spip('territoires/territoires');
	territoires_configuration_recharger('territoires');
}
