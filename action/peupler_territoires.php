<?php
/**
 * Ce fichier contient l'action `peupler_territoires` lancée de façon asynchrone pour peupler certains
 * ensembles de territoires contenant un nombre important d'éléments.
 *
 * @package SPIP\TERRITOIRES
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet peupler certains ensembles de territoires contenant un nombre important d'éléments.
 * Elle est appelée par l'API `unite_peuplement_charger_asynchrone()`.
 *
 * Elle nécessite les mêmes arguments que la fonction de peuplement.
 *
 * @uses unite_peuplement_charger()
 * @uses unite_peuplement_charger_asynchrone()
 *
 * @return void
 */
function action_peupler_territoires_dist() : void {
	// Securisation et autorisation.
	// L'argument attendu est la page à recharger ou sinon vide pour toutes les pages.
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arguments = $securiser_action();

	// Structuration des arguments de peuplement
	if (
		($arguments = explode(':', $arguments))
		and (count($arguments) > 1)
		and ($type = $arguments[0])
		and ($pays = $arguments[1])
	) {
		// Identification des options : par défaut on passe l'itération à 0
		$options = ['iteration' => 0];
		unset($arguments[0], $arguments[1]);
		foreach ($arguments as $_argument) {
			if ($_argument === 'force') {
				$options[$_argument] = true;
			} elseif (substr($_argument, 0, 4) === 'iter') {
				$options['iteration'] = (int) (str_replace('iter', '', $_argument));
			} else {
				$options['extras'][] = $_argument;
			}
		}

		// Chargement des territoires et autres extras requis par les arguments.
		include_spip('inc/territoires_unite_peuplement');
		$retour = unite_peuplement_charger($type, $pays, $options);

		// Si le nombre d'itération est fourni dans le retour de la fonction et est supérieure à 0 alors c'est qu'il faut
		// continuer le traitement.
		// Par mesure de sécurité on s'interdit de boucler plus de 50 fois.
		if (
			!empty($retour['iter'])
			and ($iteration = $retour['iter'])
			and ($iteration < 50)
		) {
			// On fournit le nombre courant d'itérations et on rappelle la fonction asynchrone pour revenir dans l'action.
			$options['iteration'] = $iteration;
			unite_peuplement_charger_asynchrone($type, $pays, $options);
		}
	}
}
