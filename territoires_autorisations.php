<?php
/**
 * Définit les autorisations du plugin Territoires.
 *
 * @package    SPIP\TERRITOIRES\AUTORISATION
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'appel pour le pipeline.
 *
 * @pipeline autoriser
 */
function territoires_autoriser() {
}

// -----------------
// Objet territoires

/**
 * Autorisation de voir la liste des territoires (page territoires).
 * Tout le monde y est autorisé.
 *
 * @param string         $faire   Action demandée : `voir`
 * @param string         $type    Type d'objet sur lequel appliquer l'action : `_territoires` (ce n'est pas un objet au sens SPIP)
 * @param int            $id      Identifiant de l'objet : `0`, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_territoires_voir_dist($faire, $type, $id, $qui, $options) {
	return true;
}

/**
 * Autorisation de voir l'élément de menu menant à la page des territoires.
 * Il faut être autorisé à voir la liste des territoires.
 *
 * @param string         $faire   Action demandée : `menu`
 * @param string         $type    Type d'objet sur lequel appliquer l'action : `_territoires` (ce n'est pas un objet au sens SPIP)
 * @param int            $id      Identifiant de l'objet : `0`, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_territoires_menu_dist($faire, $type, $id, $qui, $options) {
	return autoriser('voir', '_territoires');
}

/**
 * Autorisation de configurer le plugin (page configurer_territoires).
 * Il faut posséder l'autorisation standard de configuration.
 *
 * @param string         $faire   Action demandée : `configurer`
 * @param string         $type    Type d'objet sur lequel appliquer l'action : `_territoires` (ce n'est pas un objet au sens SPIP)
 * @param int            $id      Identifiant de l'objet : `0`, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_territoires_configurer_dist($faire, $type, $id, $qui, $options) {
	return autoriser('configurer');
}

/**
 * Autorisation de peupler ou dépeupler des territoires.
 * Il faut posséder l'autorisation standard de configuration.
 *
 * @param string         $faire   Action demandée : `peupler` (idem que depleupler)
 * @param string         $type    Type d'objet sur lequel appliquer l'action : `_territoires` (ce n'est pas un objet au sens SPIP)
 * @param int            $id      Identifiant de l'objet : `0`, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_territoires_peupler_dist($faire, $type, $id, $qui, $options) {
	return autoriser('configurer');
}

/**
 * Autorisation de voir (territoire).
 * Tout le monde y est autorisé.
 *
 * @param string         $faire   Action demandée : `voir`
 * @param string         $type    Type d'objet sur lequel appliquer l'action : objet `territoire`
 * @param int            $id      Identifiant de l'objet : `0`, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_territoire_voir_dist($faire, $type, $id, $qui, $options) {
	return true;
}

/**
 * Autorisation de créer un territoire.
 * Il n'est pas possible de créer un territoire, seule la fonction de peuplement le peut.
 *
 * @param string         $faire   Action demandée : `creer`
 * @param string         $type    Type d'objet sur lequel appliquer l'action : objet `territoire`
 * @param int            $id      Identifiant de l'objet : `0`, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_territoire_creer_dist($faire, $type, $id, $qui, $options) {
	return false;
}

/**
 * Autorisation de modifier un territoire.
 * Il faut être administrateur ou rédacteur pour être autorisé.
 *
 * @param string         $faire   Action demandée : `modifier`
 * @param string         $type    Type d'objet sur lequel appliquer l'action : objet `territoire`
 * @param int            $id      Identifiant de l'objet : `0`, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_territoire_modifier_dist($faire, $type, $id, $qui, $options) {
	return in_array($qui['statut'], ['0minirezo', '1comite']);
}

/**
 * Autorisation de supprimer un territoire.
 * Il n'est pas possible de supprimer un territoire, seule la fonction de dépeuplement le peut.
 *
 * @param string         $faire   Action demandée : `supprimer`
 * @param string         $type    Type d'objet sur lequel appliquer l'action : objet `territoire`
 * @param int            $id      Identifiant de l'objet : `0`, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_territoire_supprimer_dist($faire, $type, $id, $qui, $options) {
	return false;
}

/**
 * Autorisation de lier/délier un territoire.
 * Il faut être un administrateur complet.
 *
 * @param string         $faire   Action demandée : `associerterritoires`
 * @param string         $type    Type d'objet sur lequel appliquer l'action : aucun
 * @param int            $id      Identifiant de l'objet : `0`, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_associerterritoires_dist($faire, $type, $id, $qui, $options) {
	return $qui['statut'] == '0minirezo' and !$qui['restreint'];
}
