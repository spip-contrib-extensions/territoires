<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Territoires.
 *
 * @package    SPIP\TERRITOIRES\INSTALLATION
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'installation et de mise à jour du plugin Territoires.
 *
 * @param string $nom_meta_base_version Nom de la meta informant de la version du schéma de données du plugin
 *                                      installé dans SPIP
 * @param string $version_cible         Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 *
 * @return void
**/
function territoires_upgrade(string $nom_meta_base_version, string $version_cible) : void {
	$maj = [];

	// Initialisation de la configuration
	// -- Configuration statique du plugin Territoires
	$config_statique = territoires_configurer('statique');
	// -- Configuration dynamique du plugin Territoires
	$config_modifiable = territoires_configurer('utilisateur');

	$maj['create'] = [
		['maj_tables', ['spip_territoires', 'spip_territoires_extras', 'spip_territoires_liens']],
		['ecrire_config', 'territoires', array_merge($config_statique, $config_modifiable)],
		['territoires_chargement_initial']
	];

	// Ajout des extras pour les zones (code geoIP des continents).
	$maj['2'] = [
		['territoires_adapter_config_statique',	$config_statique, $config_modifiable]
	];

	// Ajout de la colonne profondeur_type et de la configuration associée pour son importation de Nomenclatures.
	$maj['3'] = [
		['sql_alter', 'TABLE spip_territoires ADD COLUMN profondeur_type int DEFAULT 0 NOT NULL AFTER iso_parent'],
		['territoires_adapter_config_statique',	$config_statique, $config_modifiable]
	];

	// Ajout de la liste des types de territoire
	$maj['4'] = [
		['territoires_adapter_config_statique',	$config_statique, $config_modifiable]
	];

	// Ajout des coordonnées géographiques comme extras des infrasubdivisions
	$maj['5'] = [
		['territoires_adapter_config_statique',	$config_statique, $config_modifiable]
	];

	// Ajout des coordonnées géographiques comme extras des infrasubdivisions
	$maj['6'] = [
		['sql_alter', 'TABLE spip_territoires ADD COLUMN iso_parent_alt varchar(20) NOT NULL DEFAULT "" AFTER profondeur_type'],
		['sql_alter', 'TABLE spip_territoires ADD INDEX iso_parent_alt (iso_parent_alt)'],
		['territoires_adapter_config_statique',	$config_statique, $config_modifiable]
	];

	// Changement d'un nom de nature d'extra (date_creation)
	$maj['7'] = [
		['territoires_adapter_config_statique',	$config_statique, $config_modifiable]
	];

	// Transfert de la meta de peuplement vers 4 metas, une par type
	$maj['8'] = [
		['territoires_adapter_config_statique',	$config_statique, $config_modifiable]
		['territoires_maj_8_meta_peuplement']
	];

	// Ajout de la confguration initiale des unites qui pourra être développée par les utilisateurs
	$maj['9'] = [
		['territoires_maj_9_unite', $config_modifiable]
	];

	// Retrait de la population des extras des pays
	$maj['10'] = [
		['territoires_adapter_config_statique',	$config_statique, $config_modifiable]
	];

	// Ajout du code postal des communes françaises
	$maj['11'] = [
		['territoires_adapter_config_statique',	$config_statique, $config_modifiable]
	];

	// Ajout du code postal des communes françaises
	$maj['12'] = [
		['territoires_maj_12_info', $config_statique, $config_modifiable]
	];

	// Création des tables et de la configuration du plugin
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Fonction de désinstallation du plugin Territoires.
 *
 * @param string $nom_meta_base_version Nom de la meta informant de la version du schéma de données du plugin
 *                                      installé dans SPIP
 *
 * @return void
**/
function territoires_vider_tables(string $nom_meta_base_version) : void {
	// Supprimer les tables ajoutées par le plugin
	sql_drop_table('spip_territoires');
	sql_drop_table('spip_territoires_extras');
	sql_drop_table('spip_territoires_liens');

	// Nettoyer les liens courants (le génie optimiser_base_disparus se chargera de nettoyer toutes les tables de liens)
	sql_delete('spip_documents_liens', sql_in('objet', ['territoire']));
	sql_delete('spip_mots_liens', sql_in('objet', ['territoire']));
	sql_delete('spip_auteurs_liens', sql_in('objet', ['territoire']));
	// Nettoyer les versionnages
	sql_delete('spip_versions', sql_in('objet', ['territoire']));
	sql_delete('spip_versions_fragments', sql_in('objet', ['territoire']));
	sql_delete('spip_forum', sql_in('objet', ['territoire']));

	// Effacer les meta du plugin
	// -- configuration du plugin
	effacer_meta('territoires');
	// -- ancienne variable de consignation des peuplements
	effacer_meta('territoires_peuplement');
	// -- nouvelles variables de consignation des peuplements
	include_spip('inc/unite_peuplement');
	include_spip('inc/config');
	$types = lire_config('territoires/types', []);
	foreach ($types as $_type) {
		effacer_meta(unite_peuplement_consigne_identifier('territoires', $_type));
	}
	// -- schéma du plugin
	effacer_meta($nom_meta_base_version);
}

/**
 * Renvoie la configuration du plugin, soit statique (non modifiable), soit utilisateur (modifiable).
 *
 * @api
 *
 * @param null|string $type_configuration Type de configuration. Prend les valeurs `statique` ou `utilisateur`.
 *
 * @return array Tableau de la configuration demandée.
 */
function territoires_configurer(?string $type_configuration = 'statique') : array {
	if ($type_configuration === 'statique') {
		// Configuration de l'extraction des données REST
		$configuration = [
			// configuration de l'extraction des territoires de type zone
			'zone' => [
				// -- liste des champs récupérés dans la réponse.
				'champs' => [
					// L'index 'base' correspond aux colonnes dans la table spip_territoires sous la forme :
					// [index dans la réponse] = colonne de spip_territoires
					'base' => [
						'code_num' => 'iso_territoire',
						'category' => 'categorie',
						'parent'   => 'iso_parent',
						'depth'    => 'profondeur_type',
						'label'    => 'iso_titre',
					],
					// L'index 'index' définit la racine des champs dans la réponse
					'index' => 'zones',
				],
				// -- liste des extras de type 'code' ou 'info' récupérés dans la réponse à un index différent de
				//    celui des champs et mis à jour dans la table spip_territoires_extras
				'extras' => [
					// Pour le type 'code'
					'code' => [
						// L'index 'champs' correspond aux colonnes dans la table spip_territoires_extras sous la forme :
						// [colonne de spip_territoires_extras] = index dans la réponse
						'champs' => [
							'extra'  => '#code_geoip', // La réponse ne contient pas ce champ : il est forcé à 'code_geopip'
							'valeur' => 'code',
						],
						// L'index 'index' définit la racine des champs extras dans la réponse
						'index' => 'continents',
						// L'index 'cle_iso' identifie l'index dans la réponse qui représente le code iso, soit iso_territoire
						'cle_iso' => 'code_num',
					],
				],
				'index_hash'           => 'hash',
				'collection'           => 'zones',
				'populated_by_country' => false,
			],
			'country' => [
				'champs' => [
					'base' => [
						'code_alpha2'    => 'iso_territoire',
						'category'       => 'categorie',
						'code_continent' => 'iso_continent',
						'label'          => 'iso_titre',
					],
					// -- liste des extras de type 'code' récupérés dans la réponse au même index que
					//    celui des champs et aussi mis à jour dans la table spip_territoires_extras.
					//    La forme des correspondance est identique à celle des champs de base.
					'extras' => [
						'code' => [
							'code_alpha3' => 'code_iso3166_a3',
							'code_num'    => 'code_iso3166_num',
						],
					],
					'index' => 'pays',
				],
				'index_hash'           => 'hash',
				'collection'           => 'pays',
				'populated_by_country' => false,
			],
			'subdivision' => [
				'champs' => [
					'base' => [
						'code_3166_2' => 'iso_territoire',
						'type'        => 'categorie',
						'country'     => 'iso_pays',
						'parent'      => 'iso_parent',
						'depth'       => 'profondeur_type',
						'label'       => 'iso_titre',
					],
					'index' => 'subdivisions',
				],
				'extras' => [
					'code' => [
						'champs' => [
							'extra'  => 'type_alter',
							'valeur' => 'code_alter',
						],
						'index'   => 'codes_alternatifs',
						'cle_iso' => 'code_iso',
					],
				],
				'index_hash'           => 'hash_by_country',
				'collection'           => 'subdivisions',
				'populated_by_country' => true,
			],
			'infrasubdivision' => [
				'champs' => [
					'base' => [
						'code'        => 'iso_territoire',
						'type'        => 'categorie',
						'country'     => 'iso_pays',
						'parent'      => 'iso_parent',
						'depth'       => 'profondeur_type',
						'label'       => 'iso_titre',
						'parent_alt1' => 'iso_parent_alt',
					],
					'extras' => [
						'code' => [
							'postcode' => 'code_postal',
						],
					],
					'index' => 'infrasubdivisions',
				],
				'extras' => [
					'code' => [
						'champs' => [
							'extra'  => 'type_alter',
							'valeur' => 'code_alter',
						],
						'index'   => 'codes_alternatifs',
						'cle_iso' => 'code_iso',
					],
				],
				'index_hash'           => 'hash',
				'collection'           => 'infrasubdivisions',
				'populated_by_country' => true,
			],
			'protected_area' => [
				'champs' => [
					'base' => [
						'code_wdpa' => 'iso_territoire',
						'type'      => 'categorie',
						'country'   => 'iso_pays',
						'parent'    => 'iso_parent',
						'label'     => 'iso_titre',
					],
					'index' => 'protected_areas',
				],
				'extras' => [
					'code' => [
						'champs' => [
							'extra'  => 'type_alter',
							'valeur' => 'code_alter',
						],
						'index'   => 'codes_alternatifs',
						'cle_iso' => 'code_iso',
					],
				],
				'index_hash'           => 'hash_by_country',
				'collection'           => 'protected_areas',
				'populated_by_country' => true,
			],
		];
		// liste des types de territoires : les index des configurations
		$configuration['types'] = array_keys($configuration);
	} else {
		// Liste des objets associables à un territoire
		$configuration = [
			'association_objets' => [],
		];
	}

	return $configuration;
}

/**
 * Fonction de mise à jour de la config statique du plugin.
 *
 * @param array $config_statique   Nouvelle configuration statique
 * @param array $config_modifiable Initialisation de la configuration modifiable (paramétrage)
 *
 * @return void
**/
function territoires_adapter_config_statique(array $config_statique, array $config_modifiable) : void {
	// Configuration actuelle pour conserver la config modifiable
	include_spip('inc/config');
	$config = lire_config('territoires', []);

	if (!$config) {
		// On charge la configuration initiale car rien n'est encore configuré
		$config = array_merge($config_statique, $config_modifiable);
	} else {
		// On insère la nouvelle config statique
		foreach ($config as $_cle => $_config) {
			if (isset($config_statique[$_cle])) {
				// On remplace la config statique et on retire cet index de la nouvelle config statique
				$config[$_cle] = $config_statique[$_cle];
				unset($config_statique[$_cle]);
			} elseif (!isset($config_modifiable[$_cle])) {
				// On supprime la clé statique qui n'existe plus
				unset($config[$_cle]);
			}
			// On ajoute les nouveaux index statiques
			$config = array_merge($config, $config_statique);
		}
	}

	// Mise à jour en meta
	ecrire_config('territoires', $config);
}

/**
 * Lance le chargement initial des unités de peuplement des régions du monde, des pays et des subdivisions et
 * des infra-subbdivisions françaises.
 *
 * @return void
**/
function territoires_chargement_initial() : void {
	// Peuplement minimal du plugin en synchrone:
	// - les zones du monde
	// - les pays
	// - les subdivisions françaises
	include_spip('inc/territoires_unite_peuplement');
	$options['extras'] = ['code', 'info'];
	unite_peuplement_charger('zone', '', $options);
	unite_peuplement_charger('country', '', $options);
	unite_peuplement_charger('subdivision', 'FR', $options);
	// Peuplement en asynchrone:
	// - les infrasubdivisions françaises
	unite_peuplement_charger_asynchrone('infrasubdivision', 'FR', $options);
}

/**
 * Transfert de la meta de peuplement vers n metas, une par type.
 *
 * @return void
**/
function territoires_maj_8_meta_peuplement() : void {
	// Configuration actuelle pour conserver la config modifiable
	include_spip('inc/config');
	$meta = lire_config('territoires_peuplement', []);

	// On insère la nouvelle config statique
	if ($meta) {
		foreach ($meta as $_type => $_contenu) {
			ecrire_config("territoires_peuplement_{$_type}", $_contenu);
		}
	}

	// Effacer l'ancienne meta
	effacer_meta('territoires_peuplement');
}

/**
 * Prise en compte des nouveaux paramètres pour les unités.
 *
 * @param array $config_modifiable
*
 * @return void
**/
function territoires_maj_9_unite(array $config_modifiable) : void {
	// Configuration complète du plugin (statique et modifiable)
	include_spip('inc/config');
	$meta = lire_config('territoires', []);

	// On insère la nouvelle config modifiable
	foreach ($config_modifiable as $_cle => $_valeur) {
		if (!isset($meta[$_cle])) {
			$meta[$_cle] = $_valeur;
		}
	}

	ecrire_config('territoires', $meta);
}

/**
 * Prise en compte des nouveaux paramètres pour les unités.
 *
 * @param array $config_statique   Nouvelle configuration statique
 * @param array $config_modifiable Initialisation de la configuration modifiable (paramétrage)
 *
 * @return void
**/
function territoires_maj_12_info(array $config_statique, array $config_modifiable) : void {
	// Mise à jour de la configuration statique
	territoires_adapter_config_statique($config_statique, $config_modifiable);

	// Mise à jour de la configuration modifiable (suppression d'index)
	include_spip('inc/config');
	$config = lire_config('territoires', []);
	foreach ($config as $_cle => $_valeur) {
		if (
			!array_key_exists($_cle, $config_statique)
			and !array_key_exists($_cle, $config_modifiable)
		) {
			unset($config[$_cle]);
		}
	}
	ecrire_config('territoires', $config);

	// Suppression des extras de type 'info' de la table des extras et des consignations :
	// -- les extras de la table spip_territoires_extras sont tous sans feed_id aujourd'hui, il est donc inutile d'en tenir compte
	$where = [
		'type_extra=' . sql_quote('info')
	];
   	sql_delete('spip_territoires_extras', $where);
	// -- Déconsigner les extras de type info
	include_spip('inc/unite_peuplement');
	$types = lire_config('territoires/types', []);
	foreach ($types as $_type) {
		$id_consigne = unite_peuplement_consigne_identifier('territoires', $_type);
		$peuplement = lire_config($id_consigne, []);
		if (lire_config("territoires/{$_type}/populated_by_country", false)) {
			foreach ($peuplement as $_pays => $_peuplement_pays) {
				$info_index = array_search('info', $_peuplement_pays['ext']);
				if ($info_index !== false) {
					unset($peuplement[$_pays]['ext'][$info_index]);
				}
			}
			ecrire_config($id_consigne, $peuplement);
		} else {
			$info_index = array_search('info', $peuplement['ext']);
			if ($info_index !== false) {
				unset($peuplement['ext'][$info_index]);
				ecrire_config($id_consigne, $peuplement);
			}
		}
	}
}
