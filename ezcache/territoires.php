<?php
/**
 * Ce fichier contient les fonctions de service nécessitées par l'utilisation du plugin Cache Factory.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie la configuration spécifique des caches de Rainette.
 *
 * @param string $plugin Préfixe du plugin, à savoir, `territoires`.
 *
 * @return array<string, mixed> Tableau de la configuration brute du plugin Territoires
 */
function territoires_cache_configurer(string $plugin) : array {
	// Initialisation du tableau de configuration avec les valeurs par défaut du plugin Cache.
	$configuration = [
		'asynchrone' => [
			'racine'          => '_DIR_TMP',
			'sous_dossier'    => true,
			'nom_obligatoire' => ['fonction', 'type', 'pays'],
			'nom_facultatif'  => ['service'],
			'extension'       => '.php',
			'securisation'    => true,
			'serialisation'   => true,
			'separateur'      => '-',
			'conservation'    => 0
		],
		'synchrone' => [
			'racine'          => '_DIR_VAR',
			'sous_dossier'    => true,
			'nom_obligatoire' => ['type', 'pays'],
			'nom_facultatif'  => ['service'],
			'extension'       => '.json',
			'securisation'    => false,
			'serialisation'   => false,
			'decodage'        => true,
			'separateur'      => '-',
			'conservation'    => 3600 * 24 * 7
		]
	];

	return $configuration;
}
