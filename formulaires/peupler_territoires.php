<?php
/**
 * Gestion du formulaire de chargement ou de vidage des unités de peuplement de territoires.
 *
 * @package SPIP\TERRITOIRES\UI
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement des données : le formulaire propose les actions possibles sur les territoires,
 * à savoir, charger ou vider.
 *
 * @uses unite_peuplement_serveur_est_compatible()
 * @uses unite_peuplement_informer_feeds()
 * @uses territoire_repertorier()
 * @uses unite_peuplement_est_chargee()
 * @uses unite_peuplement_est_obsolete()
 *
 * @param string $groupe Groupe de peuplement : représente les types de territoires à présenter dans un même formulaire.
 *
 * @return array Tableau des données à charger par le formulaire (affichage). Aucune donnée chargée n'est un
 *               champ de saisie, celle-ci sont systématiquement remises à zéro.
 *               - `_actions_territoire`: (affichage) alias et libellés des actions possibles, `charger` et `vider`
 *               - `_action_defaut`     : (affichage) action sélectionnée par défaut, `charger`
 *               - `_donnees`           : (affichage) liste des ensembles de territoires pouvant être chargés.
 *               - `_explication_infra` : (affichage) explication sur les infrasubdivisions.
 *               - `_classe_conteneur`  : (affichage) classe pour forcer un conteneur flex pour les checkbox.
 *               - `_max_choix`         : (affichage) limiter le nombre de choix à 1 pour les infrasubdivisions.
 */
function formulaires_peupler_territoires_charger(string $groupe) : array {
	// Initialisation des valeurs à transmettre au formulaire
	$valeurs = [
		'_groupe'             => $groupe,
		'_explication'        => '',
		'_attention'          => '',
		'_actions_territoire' => [],
		'_action_defaut'      => 'charger',
		'_explication_infra'  => '',
		'_classe_conteneur'   => '',
		'_choix_multi_col'    => 0,
		'_max_choix'          => null,
		'_donnees'            => [],
		'editable'            => true,
	];

	// Vérifier que la version du serveur est compatible avec celle du plugin Territoires.
	// Si ce n'est pas le cas, on affiche un message d'erreur et on rend le formulaire non éditable.
	include_spip('inc/unite_peuplement');
	if (unite_peuplement_serveur_est_compatible('territoires')) {
		// Explication générale du formulaire
		$valeurs['_explication'] = _T('territoires:explication_peupler_form');

		// Lister les actions sur les territoires
		// -- le groupe 'zone_country' des régions et pays ne doit jamais être supprimé
		$valeurs['_actions_territoire'] = [
			'charger' => _T('territoires:option_peupler_action')
		];
		if ($groupe !== 'zone_country') {
			$valeurs['_actions_territoire']['vider'] = _T('territoires:option_depeupler_action');
		}

		// Initialiser la liste des territoires à peupler suivant le groupe de types passé en argument du formulaires
		if ($groupe === 'zone_country') {
			// Remplissage des données pour le groupe
			$valeurs['_donnees'] = [
				'zone'    => _T('territoires:option_zone_toutes'),
				'country' => _T('territoires:option_pays_tous'),
			];
		} else {
			// Acquérir les feeds fournisseurs de nomenclatures de territoire
			$pays_feeds = unite_peuplement_informer_feeds('territoires', $groupe, '', '', 'pays');

			// Extraction de la liste des pays [code] = nom d'usage
			// -- le groupe correspond au type de territoire
			include_spip('inc/territoire');
			$filtres = [
				'iso_territoire' => implode(',', $pays_feeds)
			];
			$pays = territoire_repertorier($filtres);

			// Remplissage des données pour le groupe
			include_spip('inc/filtres');
			foreach ($pays as $_code => $_pays) {
				if (
					($groupe !== 'infrasubdivision')
					or unite_peuplement_est_chargee('territoires', 'subdivision', $_code)
					or unite_peuplement_est_chargee('territoires', 'protected_area', $_code)
				) {
					$nom_pays = extraire_multi($_pays['nom_usage']);
					$valeurs['_donnees'][$_code] = "{$_code} - {$nom_pays}";
				}
			}

			// Attribution des variables spécifiques à chaque groupe
			$valeurs['_choix_multi_col'] = 1 + intdiv(count($pays), 10);
			if ($valeurs['_choix_multi_col'] > 3) {
				$valeurs['_choix_multi_col'] = 3;
			}
			$valeurs['_classe_conteneur'] = ($valeurs['_choix_multi_col'] > 1)
				? 'pleine_largeur'
				: '';
			if ($groupe === 'infrasubdivision') {
				$valeurs['_explication_infra'] = _T('territoires:explication_infrasubdivision_territoire');
				$valeurs['_max_choix'] = 1;
			}
		}

		// Indiquer les territoires déjà chargés et ceux possédant une mise à jour
		foreach ($valeurs['_donnees'] as $_cle => $_donnee) {
			if (
				(
					($groupe === 'zone_country')
					and unite_peuplement_est_chargee('territoires', $_cle)
				)
				or (
					($groupe !== 'zone_country')
					and unite_peuplement_est_chargee('territoires', $groupe, $_cle)
				)
			) {
				$est_obsolete = $groupe === 'zone_country'
					? unite_peuplement_est_obsolete('territoires', $_cle)
					: unite_peuplement_est_obsolete('territoires', $groupe, $_cle);
				$valeurs['_donnees'][$_cle] .= ' - <em>['
					. _T('territoires:info_territoire_peuple')
					. ($est_obsolete ? ', ' . _T('territoires:info_territoire_obsolete') : '')
					. ']</em>';
			}
		}

		// Ne pas sélectionner un ensemble de territoires par défaut
		set_request('territoires', []);

		// Si aucune unité de peuplement n'est identifiée, alors on rend le formulaire non éditable
		if (!$valeurs['_donnees']) {
			$valeurs['editable'] = false;
		}
	} else {
		// Expliquer le problème
		$valeurs['_attention'] = _T('territoires:msg_serveur_erreur');
		$valeurs['editable'] = false;
	}

	return $valeurs;
}

/**
 * Vérification des saisies : il est indispensable de choisir une action (`charger` ou `vider`) et
 * au moins un ensemble de territoire.
 *
 * @param string $groupe Groupe de peuplement : représente les types de territoires à présenter dans un même formulaire.
 *
 * @return array Tableau des erreurs sur l'action et/ou le pays ou tableau vide si aucune erreur.
 */
function formulaires_peupler_territoires_verifier(string $groupe) : array {
	$erreurs = [];

	if (!_request('action_territoire')) {
		$erreurs['action_territoire'] = _T('info_obligatoire');
	}

	if (!_request('territoires')) {
		$erreurs['territoires'] = _T('info_obligatoire');
	}

	return $erreurs;
}

/**
 * Exécution du formulaire : les territoires choisis sont soit vidés, soit chargés.
 *
 * @uses unite_peuplement_charger()
 * @uses unite_peuplement_vider()
 * @uses peupler_territoires_notifier()
 *
 * @param string $groupe Groupe de peuplement : représente les types de territoires à présenter dans un même formulaire.
 *
 * @return array Tableau retourné par le formulaire contenant toujours un message de bonne exécution ou
 *               d'erreur. L'indicateur editable est toujours à vrai.
 */
function formulaires_peupler_territoires_traiter(string $groupe) : array {
	// Initialisation des messages de retour
	$retour = [
		'message_ok'     => '',
		'message_erreur' => ''
	];

	// Acquisition des saisies: comme elles sont obligatoires, il existe toujours une action et un territoire
	// à savoir soit un type région ou pays, soit un pays pour une subdivision ou une infrasubdivision.
	$action = _request('action_territoire');
	$territoires_a_peupler = _request('territoires');

	// Détermination des types de territoire et des pays concernés par les saisies
	$types = [];
	$pays = [];
	if ($groupe === 'zone_country') {
		$types = $territoires_a_peupler;
	} else {
		$types[] = $groupe;
		$pays = $territoires_a_peupler;
	}

	// Détermination des options de l'action :
	// -- depuis le refactoring du plugin, les extras de type info ne sont plus disponibles dans Territoires.
	//    On ne propose plus de choisir les extras, on insère systématiquement les codes.
	$options = [
		'extras' => ['code'],
		'forcer' => false
	];
	if ($action === 'vider') {
		$options['force'] = true;
	}

	// On peuple chaque type (ou type,pays pour les subdivisions).
	// (La fonction de chargement lance un vidage préalable si le pays demandé est déjà chargée)
	include_spip('inc/territoires_unite_peuplement');
	$actionner = "unite_peuplement_{$action}";
	$statut = [];
	foreach ($types as $_type) {
		// Traitement du type en prenant en compte le cas particulier des subdivisions et infra subdivisions.
		if ($groupe === 'zone_country') {
			$statut[] = $actionner($_type, '', $options);
		} else {
			if (
				($_type === 'infrasubdivision')
				and ($action === 'charger')
			) {
				$actionner .= '_asynchrone';
			}
			foreach ($pays as $_pays) {
				$statut[] = $actionner($_type, $_pays, $options);
			}
		}

		// Formater le message correspondant au traitement du type
		$retour = peupler_territoires_notifier($_type, $retour, $action, $statut);
	}

	// Formulaire toujours éditable
	$retour['editable'] = true;

	return $retour;
}

/**
 * Formate les messages de succès et d'erreur résultant des actions de chargement ou de vidage
 * des territoires.
 *
 * @param string $type     Type de territoire.
 * @param array  $messages Tableau des messages ok et nok à compléter.
 * @param string $action   Action venant d'être appliquée à certains pays. Peut prendre les valeurs `peupler` et
 *                         `depeupler`.
 * @param array  $statuts  Tableau résultant de l'action sur le type choisi. Peut-êre un tableau de statut pour les
 *                         subdivisions (plusieurs pays).
 *                         - `ok`   : `true` si l'action ou la lancement du job a complètement réussi, `false` sinon (au moins une erreur).
 *                         - `sha`  : indique une sha identique donc pas chargement effectué.
 *                         - `arg`  : indique que le couple (type, pays) est invalide (pas possible avec le formulaire).
 *                         - `type` : type de territoire.
 *                         - `pays` : code ISO alpha2 du pays si le type est subdivision.
 *                         - `sync` : indique si le peuplement est synchrone ou asynchrone
 *                         - `job`  : id du job si asynchrone
 *
 * @return array Tableau des messages à afficher sur le formulaire:
 *               - `message_ok`     : message sur les types ayant été traités avec succès ou vide sinon.
 *               - `message_erreur` : message sur les types en erreur ou vide sinon.
 */
function peupler_territoires_notifier(string $type, array $messages, string $action, array $statuts) : array {
	$variables = [
		'ok'  => [],
		'nok' => [],
		'sha' => [],
	];
	$statut_global = [
		'ok'   => false,
		'nok'  => false,
		'sha'  => false,
		'sync' => true,
	];

	// On compile la liste des pays traités et un indicateur global pour chaque cas d'erreur.
	include_spip('inc/config');
	$est_peuple_par_pays = lire_config("territoires/{$type}/populated_by_country", false);
	foreach ($statuts as $_statut) {
		// Traitement des succès
		if (!empty($_statut['sha'])) {
			if ($est_peuple_par_pays) {
				$variables['sha'][] = $_statut['pays'];
			}
			$statut_global['sha'] = true;
		} elseif (!$_statut['ok']) {
			if ($est_peuple_par_pays) {
				$variables['nok'][] = $_statut['pays'];
			}
			$statut_global['nok'] = true;
		} else {
			if ($est_peuple_par_pays) {
				$variables['ok'][] = $_statut['pays'];
			}
			$statut_global['ok'] = true;
			if (!$_statut['sync']) {
				$statut_global['sync'] = false;
			}
		}
	}

	// Traitement des succès
	if ($statut_global['ok']) {
		$messages['message_ok'] .= _T("territoires:msg_{$action}_{$type}_succes", ['pays' => implode(', ', $variables['ok'])]);
		if (!$statut_global['sync']) {
			$messages['message_ok'] .= '<br />' . _T('territoires:msg_succes_asynchrone');
		}
	}

	// Traitement des erreurs
	if ($statut_global['nok']) {
		$messages['message_erreur'] .= _T("territoires:msg_{$action}_{$type}_erreur", ['pays' => implode(', ', $variables['nok'])]);
	}
	if ($statut_global['sha']) {
		$messages['message_erreur'] .= $messages['message_erreur'] ? '<br />' : '';
		$messages['message_erreur'] .= _T("territoires:msg_{$action}_{$type}_notice", ['pays' => implode(', ', $variables['sha'])]);
	}

	return $messages;
}
