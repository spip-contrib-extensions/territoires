<?php
/**
 * Gestion du formulaire de d'édition de territoire.
 *
 * @package    SPIP\TERRITOIRES\UI
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité.
 *
 * @param int|string $id_territoire  Identifiant du territoire. 'new' pour un nouveau territoire.
 * @param string     $retour         URL de redirection après le traitement
 * @param string     $associer_objet Éventuel `objet|x` indiquant de lier le territoire créé à cet objet,
 *                                   tel que `article|3`
 * @param int        $lier_trad      Identifiant éventuel d'un territoire source d'une traduction
 * @param string     $config_fonc    Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array      $row            Valeurs de la ligne SQL du territoire, si connu
 * @param string     $hidden         Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 *
 * @return string Hash du formulaire
 */
function formulaires_editer_territoire_identifier_dist($id_territoire = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	return serialize([(int) $id_territoire, $associer_objet]);
}

/**
 * Chargement du formulaire d'édition de territoire.
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_territoire  Identifiant du territoire. 'new' pour un nouveau territoire.
 * @param string     $retour         URL de redirection après le traitement
 * @param string     $associer_objet Éventuel `objet|x` indiquant de lier le territoire créé à cet objet,
 *                                   tel que `article|3`
 * @param int        $lier_trad      Identifiant éventuel d'un territoire source d'une traduction
 * @param string     $config_fonc    Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array      $row            Valeurs de la ligne SQL du territoire, si connu
 * @param string     $hidden         Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 *
 * @return array Environnement du formulaire
 */
function formulaires_editer_territoire_charger_dist($id_territoire = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	return formulaires_editer_objet_charger('territoire', $id_territoire, '', $lier_trad, $retour, $config_fonc, $row, $hidden);
}

/**
 * Vérifications du formulaire d'édition de territoire.
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_territoire  Identifiant du territoire. 'new' pour un nouveau territoire.
 * @param string     $retour         URL de redirection après le traitement
 * @param string     $associer_objet Éventuel `objet|x` indiquant de lier le territoire créé à cet objet,
 *                                   tel que `article|3`
 * @param int        $lier_trad      Identifiant éventuel d'un territoire source d'une traduction
 * @param string     $config_fonc    Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array      $row            Valeurs de la ligne SQL du territoire, si connu
 * @param string     $hidden         Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 *
 * @return array Tableau des erreurs
 */
function formulaires_editer_territoire_verifier_dist($id_territoire = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	return formulaires_editer_objet_verifier('territoire', $id_territoire);
}

/**
 * Traitement du formulaire d'édition de territoire.
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_territoire  Identifiant du territoire. 'new' pour un nouveau territoire.
 * @param string     $retour         URL de redirection après le traitement
 * @param string     $associer_objet Éventuel `objet|x` indiquant de lier le territoire créé à cet objet,
 *                                   tel que `article|3`
 * @param int        $lier_trad      Identifiant éventuel d'un territoire source d'une traduction
 * @param string     $config_fonc    Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array      $row            Valeurs de la ligne SQL du territoire, si connu
 * @param string     $hidden         Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 *
 * @return array Retours des traitements
 */
function formulaires_editer_territoire_traiter_dist($id_territoire = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$retours = formulaires_editer_objet_traiter('territoire', $id_territoire, '', $lier_trad, $retour, $config_fonc, $row, $hidden);

	// Un lien a prendre en compte ?
	if ($associer_objet and $id_territoire = $retours['id_territoire']) {
		[$objet, $id_objet] = explode('|', $associer_objet);

		if ($objet and $id_objet and autoriser('modifier', $objet, $id_objet)) {
			include_spip('action/editer_liens');

			objet_associer(['territoire' => $id_territoire], [$objet => $id_objet]);

			if (isset($retours['redirect'])) {
				$retours['redirect'] = parametre_url($retours['redirect'], 'id_lien_ajoute', $id_territoire, '&');
			}
		}
	}

	return $retours;
}
