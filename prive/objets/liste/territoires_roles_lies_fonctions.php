<?php
/**
 * Fonctions du squelette associé
 *
 * @plugin     Référentiels
 * @copyright  2020
 * @author     Takkan Solutions
 * @licence    GNU/GPL
 * @package    SPIP\Confbase\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// pour initiale et afficher_initiale
include_spip('prive/objets/liste/auteurs_fonctions');
