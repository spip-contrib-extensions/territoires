<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/territoires.git

return [

	// T
	'territoires_description' => 'Ce plugin fournit un nouvel objet éditorial couvrant l’ensemble des zones géographiques du continent à la subdivision d’un pays. Il est possible de peupler les zones, les pays ou les subdivisions de façon indépendante. 
	Il est possible d\\associer des contours géographiques aux territoires en utilisant le plugin Contours.',
	'territoires_nom' => 'Territoires',
	'territoires_slogan' => 'La hiérarchie des zones géographiques du continent à la subdivision minimale d’un pays',
];
