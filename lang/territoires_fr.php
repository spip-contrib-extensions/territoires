<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/territoires.git

return [

	// C
	'cfg_bouton_recharger' => 'Recharger la configuration',
	'cfg_label_objets_associes' => 'Permettre l’association de territoire à des objets',

	// E
	'explication_forcer' => 'Le forçage est utile uniquement pour ajouter après coup des identifiants ou des informations complémentaires.',
	'explication_infrasubdivision_territoire' => 'Les subdivisions parentes doivent être déjà chargées',
	'explication_peupler_form' => 'Si les territoires sont déjà chargées ils seront supprimés avant le rechargement. Néanmoins, les éventuelles modifications manuelles faites après le chargement initial et les liens avec les logos et les autres objets seront conservés.
	Le vidage lui ne conserve ni les territoires, ni les extras, ni les liens.',

	// I
	'info_enfant_aucun' => 'Aucun territoire directement rattaché',
	'info_etape_precedente' => 'Retourner à l’étape précédente',
	'info_parent_aucun' => 'Racine de la hiérarchie',
	'info_parent_non_disponible' => 'Non disponible',
	'info_territoire_obsolete' => 'à mettre à jour',
	'info_territoire_peuple' => 'chargé',

	// L
	'label_peupler_action' => 'Choisissez une action',
	'label_peupler_options' => 'Options de peuplement',
	'label_peupler_territoire' => 'Choisissez des territoires',

	// M
	'menu_configurer' => 'Gérer le paramétrage',
	'menu_lister' => 'Les territoires',
	'menu_peupler' => 'Ajouter des territoires',
	'msg_charger_country_erreur' => 'Une erreur s’est produite lors du peuplement des pays.',
	'msg_charger_country_notice' => 'Aucune mise à jour n’est nécessaire pour les pays.',
	'msg_charger_country_succes' => 'Les pays ont bien été chargés.',
	'msg_charger_infrasubdivision_erreur' => 'Une erreur s’est produite lors du peuplement des infra-subdivisions du ou des pays « @pays@ ».',
	'msg_charger_infrasubdivision_notice' => 'Aucune mise à jour n’est nécessaire pour les infra-subdivisions du ou des pays « @pays@ ».',
	'msg_charger_infrasubdivision_succes' => 'Les infra-subdivisions du ou des pays « @pays@ » ont bien été chargées.',
	'msg_charger_protected_area_erreur' => 'Une erreur s’est produite lors du peuplement des zones protégées du ou des pays « @pays@ ».',
	'msg_charger_protected_area_notice' => 'Aucune mise à jour n’est nécessaire pour les zones protégées du ou des pays « @pays@ ».',
	'msg_charger_protected_area_succes' => 'Les zones protégées du ou des pays « @pays@ » ont bien été chargées.',
	'msg_charger_subdivision_erreur' => 'Une erreur s’est produite lors du peuplement des subdivisions du ou des pays « @pays@ ».',
	'msg_charger_subdivision_notice' => 'Aucune mise à jour n’est nécessaire pour les subdivisions du ou des pays « @pays@ ».',
	'msg_charger_subdivision_succes' => 'Les subdivisions du ou des pays « @pays@ » ont bien été chargées.',
	'msg_charger_zone_erreur' => 'Une erreur s’est produite lors du peuplement des régions du monde.',
	'msg_charger_zone_notice' => 'Aucune mise à jour n’est nécessaire pour les régions du monde.',
	'msg_charger_zone_succes' => 'Les régions du monde ont bien été chargées.',
	'msg_serveur_erreur' => 'Le serveur de nomenclatures configuré n’est pas compatible avec la version courante du plugin Territoires.',
	'msg_succes_asynchrone' => 'Certains peuplements ont été lancés en asynchrone. Ils seront disponibles dans quelques instants.',
	'msg_vider_country_erreur' => 'Une erreur s’est produite lors du vidage des pays.',
	'msg_vider_country_notice' => 'Aucun vidage n’est nécessaire pour les pays.',
	'msg_vider_country_succes' => 'Les pays ont bien été vidés.',
	'msg_vider_infrasubdivision_erreur' => 'Une erreur s’est produite lors du vidage des infra-subdivisions du ou des pays « @pays@ ».',
	'msg_vider_infrasubdivision_notice' => 'Aucun vidage n’est nécessaire pour les infra-subdivisions du ou des pays « @pays@ ».',
	'msg_vider_infrasubdivision_succes' => 'Les infra-subdivisions du ou des pays « @pays@ » ont bien été vidées.',
	'msg_vider_protected_area_erreur' => 'Une erreur s’est produite lors du vidage des zones protégées du ou des pays « @pays@ ».',
	'msg_vider_protected_area_notice' => 'Aucun vidage n’est nécessaire pour les zones protégées du ou des pays « @pays@ ».',
	'msg_vider_protected_area_succes' => 'Les zones protégées du ou des pays « @pays@ » ont bien été vidées.',
	'msg_vider_subdivision_erreur' => 'Une erreur s’est produite lors du vidage des subdivisions du ou des pays « @pays@ ».',
	'msg_vider_subdivision_notice' => 'Aucun vidage n’est nécessaire pour les subdivisions du ou des pays « @pays@ ».',
	'msg_vider_subdivision_succes' => 'Les subdivisions du ou des pays « @pays@ » ont bien été vidées.',
	'msg_vider_zone_erreur' => 'Une erreur s’est produite lors du vidage des régions du monde.',
	'msg_vider_zone_notice' => 'Aucun vidage n’est nécessaire pour les régions du monde.',
	'msg_vider_zone_succes' => 'Les régions du monde ont bien été vidées.',

	// O
	'onglet_edite_non' => 'Non édités',
	'onglet_edite_oui' => 'Edités',
	'onglet_peupler_zone_country' => 'Régions & pays',
	'onglet_tous' => 'Tous',
	'option_categorie_toute' => 'Toutes les catégories',
	'option_depeupler_action' => 'Vider',
	'option_pays_tous' => 'Tous les pays',
	'option_peupler_action' => 'Peupler',
	'option_zone_toutes' => 'Toutes les régions du monde',

	// T
	'titre_etape_en_cours' => 'Etape @etape@ / @etapes@',
	'titre_form_peupler' => 'Nomenclatures des territoires',
	'titre_liste_enfant' => 'Territoires directement rattachés',
	'titre_page_configurer' => 'Paramétrage',
	'titre_page_peupler' => 'Chargement de territoires',
	'titre_page_territoires' => 'Les territoires',
];
