<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/territoires-paquet-xml-territoires?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// T
	'territoires_description' => 'This plugin provides a new editorial object covering all geographical zones from the continent to the subdivision of a country. Zones, countries and subdivisions can be populated independently. 
	Geographic boundaries can be associated with territories using the Territory Boundaries plugin.',
	'territoires_nom' => 'Territories',
	'territoires_slogan' => 'The hierarchy of geographical zones from the continent to the minimum subdivision of a country',
];
