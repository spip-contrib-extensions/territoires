<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/territoires-territoires?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// C
	'cfg_bouton_recharger' => 'Reload plugin configuration',
	'cfg_label_objets_associes' => 'Associating territories with objects',

	// E
	'explication_forcer' => 'Forcing is only useful for adding identifiers or supplementary information afterwards.',
	'explication_infrasubdivision_territoire' => 'Parent subdivisions must already be loaded',
	'explication_peupler_form' => 'If territories are already loaded, they will be deleted before reloading. However, any manual modifications made after initial loading and links to logos and other objects will be preserved.
	Emptying does not preserve territories, extras or links.',

	// I
	'info_enfant_aucun' => 'No child territory',
	'info_etape_precedente' => 'Back to previous step',
	'info_parent_aucun' => 'Hierarchy root',
	'info_parent_non_disponible' => 'Not available',
	'info_territoire_obsolete' => 'to update',
	'info_territoire_peuple' => 'loaded',

	// L
	'label_peupler_action' => 'Select an action',
	'label_peupler_options' => 'Populating options',
	'label_peupler_territoire' => 'Select territories',

	// M
	'menu_configurer' => 'Set up the plugin',
	'menu_lister' => 'The territories',
	'menu_peupler' => 'Add territories',
	'msg_charger_country_erreur' => 'A error occured when populating the countries.',
	'msg_charger_country_notice' => 'No updates are required for countries.',
	'msg_charger_country_succes' => 'The countries have been loaded.',
	'msg_charger_infrasubdivision_erreur' => 'An error has occurred when populating the subdivisions of the "@pays@" country(ies).',
	'msg_charger_infrasubdivision_notice' => 'No update is required for subdivisions of "@pays@" country(ies).',
	'msg_charger_infrasubdivision_succes' => 'The "@pays@" subdivisions have been loaded.',
	'msg_charger_protected_area_erreur' => 'An error occurred when populating the protected areas of "@pays@" country(ies).',
	'msg_charger_protected_area_notice' => 'No update is required for protected areas of "@pays@" country(ies).',
	'msg_charger_protected_area_succes' => 'The protected areas of the "@pays@" country(ies) have been loaded.',
	'msg_charger_subdivision_erreur' => 'An error occurred when populating the subdivisions of the "@pays@" country(ies).',
	'msg_charger_subdivision_notice' => 'No update is required for "@pays@" country(ies) subdivisions.',
	'msg_charger_subdivision_succes' => 'The subdivisions of the "@pays@" country(ies) have been loaded.',
	'msg_charger_zone_erreur' => 'A error occured when populating the regions of the world.',
	'msg_charger_zone_notice' => 'No update required for regions.',
	'msg_charger_zone_succes' => 'The regions have been loaded.',
	'msg_serveur_erreur' => 'The configured data server is not compatible with the current version of the Territories plugin.',
	'msg_succes_asynchrone' => 'Some populates have been run asynchronously. They will be available shortly.',
	'msg_vider_country_erreur' => 'An error occurred when emptying the countries.',
	'msg_vider_country_notice' => 'No emptying required for countries.',
	'msg_vider_country_succes' => 'The countries have been emptied.',
	'msg_vider_infrasubdivision_erreur' => 'An error occurred when emptying the infra-subdivisions of the "@pays@ " country(ies).',
	'msg_vider_infrasubdivision_notice' => 'No update is required for infra-subdivisions of "@pays@" country(ies).',
	'msg_vider_infrasubdivision_succes' => 'The "@pays@" infra-subdivisions have been emptied.',
	'msg_vider_protected_area_erreur' => 'An error has occurred when emptying the protected areas of the "@pays@" country(ies).',
	'msg_vider_protected_area_notice' => 'No update is required for protected areas of "@pays@" country(ies). ',
	'msg_vider_protected_area_succes' => 'The "@pays@" protected areas have been emptied.',
	'msg_vider_subdivision_erreur' => 'An error has occurred when emptying the subdivisions of the "@pays@" country(ies). ',
	'msg_vider_subdivision_notice' => 'No update is required for subdivisions of "@pays@" country(ies). ',
	'msg_vider_subdivision_succes' => 'The "@pays@" subdivisions have been emptied.',
	'msg_vider_zone_erreur' => 'An error has occurred when emptying the regions. ',
	'msg_vider_zone_notice' => 'No update is required for regions. ',
	'msg_vider_zone_succes' => 'The regions have been emptied.',

	// O
	'onglet_edite_non' => 'Not modified',
	'onglet_edite_oui' => 'Modified',
	'onglet_peupler_zone_country' => 'Regions & countries',
	'onglet_tous' => 'All',
	'option_categorie_toute' => 'All categories',
	'option_depeupler_action' => 'Empty',
	'option_pays_tous' => 'All countries',
	'option_peupler_action' => 'Load',
	'option_zone_toutes' => 'All regions',

	// T
	'titre_etape_en_cours' => 'Step @etape@ / @etapes@',
	'titre_form_peupler' => 'Territory nomenclature',
	'titre_liste_enfant' => 'Child territories',
	'titre_page_configurer' => 'Plugin settings',
	'titre_page_peupler' => 'Loading territories',
	'titre_page_territoires' => 'The territories',
];
