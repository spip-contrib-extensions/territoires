<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/territoires.git

return [

	// E
	'extra_code_4217_3' => 'code devise',
	'extra_code_ars' => 'code ARS',
	'extra_code_cbs' => 'code CBS',
	'extra_code_fips' => 'code FIPS',
	'extra_code_geoip' => 'code GeoIP',
	'extra_code_ins' => 'code INS',
	'extra_code_ins_reg' => 'code INS - région',
	'extra_code_insee' => 'COG INSEE',
	'extra_code_insee_reg' => 'COG INSEE - région',
	'extra_code_iso3166_a3' => 'ISO 3166-1 alpha3',
	'extra_code_iso3166_num' => 'ISO 3166-1 numéro',
	'extra_code_ktnr' => 'code OFS',
	'extra_code_mnhn_parc' => 'code MNHN',
	'extra_code_nuts' => 'code NUTS',
	'extra_code_postal' => 'code postal',

	// T
	'titre_liste_extra_code' => 'Autres identifiants',
	'type_extra_code' => 'code',
];
