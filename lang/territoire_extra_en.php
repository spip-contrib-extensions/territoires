<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/territoire_extra-territoires?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// E
	'extra_code_4217_3' => 'currency code',
	'extra_code_ars' => 'ARS code',
	'extra_code_cbs' => 'CBS code',
	'extra_code_fips' => 'FIPS code',
	'extra_code_geoip' => 'GeoIP code',
	'extra_code_ins' => 'INS code',
	'extra_code_ins_reg' => 'INS region code',
	'extra_code_insee' => 'INSEE COG',
	'extra_code_insee_reg' => 'INSEE region COG',
	'extra_code_iso3166_a3' => 'ISO 3166-1 alpha3',
	'extra_code_iso3166_num' => 'ISO 3166-1 numeric',
	'extra_code_ktnr' => 'OFS code',
	'extra_code_mnhn_parc' => 'MNHN code',
	'extra_code_nuts' => 'NUTS code',
	'extra_code_postal' => 'postal code',

	// T
	'titre_liste_extra_code' => 'Other IDs',
	'type_extra_code' => 'code',
];
