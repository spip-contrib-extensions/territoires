<?php
/**
 * Complément spécifique de l'API de gestion des unités de peuplement des territoires.
 * Concerne uniquement les données de nomenclatures et les informations additionnelles sur les territoires.
 *
 * @package SPIP\TERRITOIRES\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_TERRITOIRES_TIME_OUT_PEUPLEMENT')) {
	/**
	 * Time-out par défaut du traitement de peuplement asynchrone.
	 */
	define('_TERRITOIRES_TIME_OUT_PEUPLEMENT', 20);
}

// -----------------------------------------------------------------------
// ------------------ API SPECIFIQUES DE TERRITOIRES ---------------------
// -----------------------------------------------------------------------

/**
 * Peuple soit les régions du monde, soit les pays ou soit les subdivisions, au sens large, d'un pays.
 * La fonction utilise les données fournies par Nomenclatures.
 *
 * @api
 *
 * @uses unite_peuplement_informer_feeds()
 * @uses unite_peuplement_est_obsolete()
 * @uses unite_peuplement_consigne_identifier()
 * @uses unite_peuplement_acquerir()
 * @uses unite_peuplement_preserver_edition()
 * @uses unite_peuplement_est_chargee()
 * @uses unite_peuplement_vider()
 * @uses cache_lire()
 * @uses territoire_initialiser_enregistrement()
 * @uses territoire_fusionner_traduction()
 * @uses unite_peuplement_compiler_extra()
 * @uses cache_ecrire()
 * @uses unite_peuplement_retablir_liens()
 * @uses cache_repertorier()
 * @uses cache_vider()
 *
 * @param string      $type    Type de territoires. Prends les valeurs `zone`, `country`, `subdivision` ou `infrasubdivision`.
 * @param null|string $pays    Code ISO 3166-1 alpha2 du pays si le type est `subdivision` ou `infrasubdivision` sinon une chaine vide.
 * @param null|array  $options Tableau des options de peuplement:
 *                             - `force` : si `true` force le repeuplement même si le sha est identique (`false` par défaut).
 *                             - `extras`: tableau des types d'extras à peupler soit `code` pour les codes alternatifs
 *                                         mais d'autres valeurs sont possibles (plugins complémentaires)
 *
 * @return array Tableau retour de la fonction permettant de connaitre le résultat du traitement (utile pour l'affichage
 *               du message dans le formulaire de peuplement).
 */
function unite_peuplement_charger(string $type, ?string $pays, ?array $options = []) : array {
	// On initialise le retour à une erreur nok
	$retour = [
		'ok'   => false,
		'arg'  => false,
		'sha'  => false,
		'type' => $type,
		'pays' => $pays,
	];

	// Détermination du mode de peuplement et insertion dans le retour pour les affichages
	$peuplement_asynchrone = isset($options['iteration']);
	$retour['sync'] = !$peuplement_asynchrone;

	include_spip('inc/unite_peuplement');
	include_spip('inc/ezcache_cache');
	// Vérifier si l'unité de territoires est à recharger ou pas
	$feeds = unite_peuplement_informer_feeds('territoires', $type, $pays);
	if (
		!empty($options['force'])
		or unite_peuplement_est_obsolete('territoires', $type, $pays, '', $feeds)
	) {
		// Lecture de la configuration statique du type de territoire
		include_spip('inc/config');
		$configuration = lire_config("territoires/{$type}");

		// Identification de la meta et de la variable de consigne
		$id_consigne = unite_peuplement_consigne_identifier('territoires', $type, $pays);

		// Initialisation du numéro d'itération (toujours à 0 en mode synchrone).
		$iteration = empty($options['iteration']) ? 0 : $options['iteration'];
		$premiere_iteration = ($iteration === 0);
		// Calcul du timeout (utile uniquement en mode asynchrone)
		$timeout = time() + _TERRITOIRES_TIME_OUT_PEUPLEMENT;

		// Initialisation à vide des variables à transmettre d'une itération à une autre.
		$ids = [];
		$extras = [];
		$meta_extras = [];
		$sha_type = '';
		$territoires = [];
		$sauvegardes = [];
		$timestamp = [];

		// Phase 1 :
		// - Acquisition des données et vérification de l'utilité de les recharger ou pas.
		// - Si rechargement et première itération, on lance des pré-traitements de sauvegardes des modifications
		//   manuelles de territoires et de vidage des territoires à recharger.
		// - Si on est dans une nième itération, les pré-traitements laissent la place à la lecture des données de
		//   contexte.
		if ($premiere_iteration) {
			// Pour la première itération, mode asynchrone ou pas, on fait appel à l'API REST.
			$timestamp['debut'] = microtime(true);

			// Acquisition de la collection complete du couple (type, pays) via l'API REST
			// -- on précise si le mode est asynchrone afin de mettre en cache la collection dans ce cas.
			$options_cache = [
				'cacher_asynchrone' => $peuplement_asynchrone
			];
			$collection = unite_peuplement_acquerir('territoires', $type, $pays, '', $options_cache);

			// On extrait la liste complète des territoires
			if (!empty($collection[$configuration['champs']['index']])) {
				$territoires = $collection[$configuration['champs']['index']];
			}
			$timestamp['acquisition'] = microtime(true);

			if ($territoires) {
				// On sait que le traitement va continuer, on peut lancer les étapes préalables à l'insertion.
				// -- on préserve les éditions manuelles et les liens pour les réinjecter ensuite lors du
				//    rechargement
				$sauvegardes = unite_peuplement_preserver_edition($type, $pays, $options_cache);
				$timestamp['preservation'] = microtime(true);

				// -- on vide les territoires avant de les remettre (inutile de gérer les erreurs
				//    car l'insertion les détectera).
				//    On gère aussi les infra subdivisions qui doivent être vidées au préalable si on vide
				//    les subdivisions parents.
				if (
					($type === 'subdivision')
					and unite_peuplement_est_chargee('territoires', 'infrasubdivision', $pays)
				) {
					unite_peuplement_vider('infrasubdivision', $pays);
				}
				unite_peuplement_vider($type, $pays);
				$timestamp['vidage'] = microtime(true);
			}
		} else {
			// Nième itération : on est en mode asynchrone, on continue à partir des données stockées
			// dans des caches sécurisés.
			// -- lecture du cache de la collection complète correspondant au couple (type, pays).
			$cache = [
				'sous_dossier' => 'territoires',
				'type'         => $type,
				'pays'         => $pays,
				'fonction'     => 'collection'
			];
			$collection = cache_lire('territoires', 'asynchrone', $cache);

			// -- lecture du cache des sauvegardes.
			$cache['fonction'] = 'sauvegarde';
			$sauvegardes = cache_lire('territoires', 'asynchrone', $cache);

			// -- lecture du contexte de chargement et extraction des territoires restant à insérer
			//    Le contexte contient toujours tous les index possibles.
			$cache['fonction'] = 'contexte';
			$contexte = cache_lire('territoires', 'asynchrone', $cache);
			if (
				$collection
				and $contexte
			) {
				$territoires = array_slice(
					$collection[$configuration['champs']['index']],
					$contexte['cle_fin'] + 1,
					null,
					true
				);
				$ids = $contexte['ids_crees'];
				$extras = $contexte['extras_crees'];
				$meta_extras = $contexte['meta_extras'];
				$sha_type = $contexte['sha_type'];
				$timestamp = $contexte['timestamp'];
			} else {
				$nom_cache = cache_nommer('territoires', 'asynchrone', $cache);
				spip_log("Pas de collection ou de contexte en cache lu, fichier : '{$nom_cache}'", 'territoires' . _LOG_AVERTISSEMENT);
			}
		}

		// Phase 2 :
		// - Insertion des territoires dans la base. Cette phase pouvant être longue elle peut nécessiter plusieurs
		//   itérations au sein d'une action asynchrone.
		if ($territoires) {
			$erreur_insertion = false;
			include_spip('inc/territoire');
			foreach ($territoires as $_cle => $_territoire) {
				// On initialise le territoire avec les noms de champs de la table spip_territoires
				$territoire = territoire_initialiser_enregistrement($_territoire, $type, $pays);

				// Si le code iso_continent est rempli c'est qu'on vient de charger les pays. Comme le code
				// du continent est le code alpha2 on le remplace par le code M49 pour être homogène.
				// -- la collection pays contient pour cela un bloc 'continents'.
				if (!empty($territoire['iso_continent'])) {
					$territoire['iso_continent'] = $collection['continents'][$territoire['iso_continent']]['code_num'];
				}

				// Intégrer les éventuelles modifications sauvegardées
				if (isset($sauvegardes['editions'][$territoire['iso_territoire']])) {
					// -- descriptif en fusionnant les traductions (priorité aux sauvegardes)
					// -- remise à 'oui' de l'indicateur d'édition
					$territoire['descriptif'] = territoire_fusionner_traduction(
						$sauvegardes['editions'][$territoire['iso_territoire']]['descriptif'],
						$territoire['descriptif']
					);
					$territoire['edite'] = 'oui';
				}

				if ($id = sql_insertq('spip_territoires', $territoire)) {
					// On consigne le couple (iso, id) pour rétablir les liens si besoin
					$ids[$territoire['iso_territoire']] = $id;

					// Si demandé et que la configuration le prévoit préparer le tableau des extras inclus dans
					// le bloc principal des territoires
					if (!empty($options['extras'])) {
						$source = $_territoire;
						$source['iso_territoire'] = $territoire['iso_territoire'];
						$extras = array_merge(
							$extras,
							unite_peuplement_compiler_extra(
								$type,
								$pays,
								'interne',
								$options['extras'],
								$source,
								$configuration,
								$meta_extras
							)
						);
					}
				} else {
					spip_log("Erreur insertion en base du territoire '{$territoire['iso_territoire']}', clé {$_cle}", 'territoires' . _LOG_ERREUR);
					$erreur_insertion = true;
					break;
				}

				// Si le territoire a été inséré, on vérifie si on a atteint le timeout. Dans ce cas, on sauvegarde
				// le contexte de chargement dans un cache et on sort de la fonction en précisant que le traitement
				// n'est pas terminé. Ainsi, l'action appelante pourra réappeler cette même fonction.
				// Cela n'est valable que pour un appel asynchrone.
				if (
					$peuplement_asynchrone
					and (time() >= $timeout)
				) {
					// Consignation du contexte dans un cache
					$contexte = [
						'cle_fin'      => $_cle,
						'ids_crees'    => $ids,
						'extras_crees' => $extras,
						'meta_extras'  => $meta_extras,
						'sha_type'     => $sha_type,
						'timestamp'    => $timestamp,
					];
					$cache = [
						'sous_dossier' => 'territoires',
						'type'         => $type,
						'pays'         => $pays,
						'fonction'     => 'contexte'
					];
					cache_ecrire('territoires', 'asynchrone', $cache, $contexte);

					// Consignation du nombre d'itérations dans le retour de la fonction
					$retour['iter'] = $iteration + 1;

					return $retour;
				}
			}
			$timestamp['insertion'] = microtime(true);

			if (!$erreur_insertion) {
				// Phase 3 :
				// - on complète la collection des extras
				// - on insère tous les extras collectés pour les territoires chargés
				// - et on rétablit les liens des territoires existant avant le chargement
				if (!empty($options['extras'])) {
					// Si la configuration le prévoit préparer le tableau des extras inclus dans un bloc annexe
					if (!empty($configuration['extras'])) {
						$extras = array_merge(
							$extras,
							unite_peuplement_compiler_extra(
								$type,
								$pays,
								'externe',
								$options['extras'],
								$collection,
								$configuration,
								$meta_extras
							)
						);
					}

					// Ajouter tous les extras compilés pour le type de territoire.
					if ($extras) {
						sql_insertq_multi('spip_territoires_extras', $extras);
					}
					$timestamp['extras'] = microtime(true);
				}

				// On rétablit les liens vers les territoires et les logos
				// -- les liens avec les autres objets
				$config_lien = [
					'table'    => 'spip_territoires_liens',
					'id_table' => 'id_territoire'
				];
				unite_peuplement_retablir_liens('liens', $sauvegardes, $ids, $config_lien);

				// -- les liens avec les logos
				$config_lien = [
					'table'    => 'spip_documents_liens',
					'id_table' => 'id_objet'
				];
				unite_peuplement_retablir_liens('logos', $sauvegardes, $ids, $config_lien);
				$timestamp['retablissement'] = microtime(true);

				// Permettre à d'autres plugins de compléter le peuplement.
				// -- par exemple le plugin Contours rétablit les liens vers les contours GIS (spip_liens_gis),
				//    les objets GIS étant conservés si ils existent déjà.
				$flux = [
					'args' => [
						'type'        => $type,
						'pays'        => $pays,
						'sauvegardes' => $sauvegardes,
						'ids_crees'   => $ids,
					],
					'data' => []
				];
				pipeline('post_peupler_territoire', $flux);

				// On stocke les informations de chargement dans une meta.
				$contenu = [
					'sha' => $feeds['hash'],
					'nbr' => count($territoires),
					'maj' => date('Y-m-d H:i:s'),
					'lic' => $collection['credits'] ?? [],
					'ext' => $meta_extras,
				];
				ecrire_config($id_consigne, $contenu);
				$timestamp['fin'] = microtime(true);
			}

			// Log du traitement pour debug
			$duree = $timestamp['fin'] - $timestamp['debut'];
			spip_log("Les territoires (Type '{$type}' - Pays '{$pays}') ont été chargées en {$duree} s", 'territoires' . _LOG_INFO_IMPORTANTE);
			if (
				defined('_LOG_FILTRE_GRAVITE')
				and (_LOG_FILTRE_GRAVITE >= _LOG_DEBUG)
			) {
				$timestamp_debut = $timestamp['debut'];
				foreach ($timestamp as $_periode => $_timestamp) {
					if ($_periode !== 'debut') {
						$timestamp_fin = $_timestamp;
						$duree = ($timestamp_fin - $timestamp_debut) * 1000;
						$timestamp_debut = $timestamp_fin;
						spip_log("Période {$_periode}: {$duree} ms", 'territoires' . _LOG_DEBUG);
					}
				}
			}
		}

		// Si on arrive à ce point de sortie, c'est que le traitement est complètement fini ou est en erreur.
		// En mode asynchrone, dans les deux cas, on supprime les caches.
		if ($peuplement_asynchrone) {
			$filtres = [
				'sous_dossier' => 'territoires',
				'type'         => $type,
				'pays'         => $pays,
			];
			$caches = cache_repertorier('territoires', 'asynchrone', $filtres);
			if ($caches) {
				// Le tableau des caches est indexé par le chemin du fichier
				cache_vider('territoires', 'asynchrone', array_keys($caches));
			}
		}

		// Si aucun territoire renvoyé on stocke le cas d'erreur.
		if (!$territoires) {
			$retour['ok'] = false;
			spip_log("Aucun territoire pour (Type '{$type}' - Pays '{$pays}') retourné par Nomenclatures", 'territoires' . _LOG_ERREUR);
		} else {
			$retour['ok'] = true;
		}
	} else {
		$retour['sha'] = true;
		spip_log("Les territoires de (Type '{$type}' - Pays '{$pays}') sont inchangés", 'territoires' . _LOG_AVERTISSEMENT);
	}

	return $retour;
}

/**
 * Appelle la fonction de peuplement de territoires en asynchrone.
 *
 * @api
 *
 * @uses generer_action_auteur()
 * @uses queue_lancer_url_http_async()
 *
 * @param string     $type    Type de territoires. Prends les valeurs `zone`, `country`, `subdivision` ou `infrasubdivision`.
 * @param string     $pays    Code ISO 3166-1 alpha2 du pays si le type est `subdivision` ou `infrasubdivision` sinon une chaine vide.
 * @param null|array $options Tableau des options de peuplement:
 *                            - `force` : si `true` force le repeuplement même si le sha est identique (`false` par défaut).
 *                            - `extras`: tableau des types d'extras à peupler soit `code` pour les codes alternatifs
 *                                        mais d'autres valeurs sont possibles (plugins complémentaires)
 *
 * @return array Tableau retour de la fonction permettant de connaitre le résultat du traitement (utile pour l'affichage
 *               du message dans le formulaire de peuplement).
 */
function unite_peuplement_charger_asynchrone(string $type, string $pays, ?array $options = []) : array {
	// On initialise le retour à une ok ce qui est le cas le plus fréquent car on ne fait que créer le job.
	$retour = [
		'arg'  => false,
		'sha'  => false,
		'type' => $type,
		'pays' => $pays,
		'sync' => false
	];

	// Construction de la chaine des arguments passés à l'action.
	// -- type et pays sont toujours fournis même si le pays est la chaine vide.
	$arguments = "{$type}:{$pays}";
	// -- ajout des éventuelles options : on passe les options par leur identifiant
	if (!empty($options['force'])) {
		$arguments .= ':force';
	}
	if (!empty($options['extras'])) {
		$arguments .= ':' . implode(':', $options['extras']);
	}
	// -- ajout du numéro d'itération
	$arguments .= ':' . (empty($options['iteration']) ? 'iter0' : "iter{$options['iteration']}");

	// Génération de l'URL qui lancera le peuplement en asynchrone.
	include_spip('inc/actions');
	$url = generer_action_auteur('peupler_territoires', $arguments, '', true, 0, true);

	// Lancement du peuplement en asynchrone.
	include_spip('inc/queue');
	$retour['ok'] = queue_lancer_url_http_async($url);

	return $retour;
}

/**
 * Supprime de la base soit les régions du monde, soit les pays ou soit les subdivisions d'un pays.
 *
 * @api
 *
 * @uses unite_peuplement_est_chargee()
 * @uses unite_peuplement_consigne_identifier()
 *
 * @param string      $type    Type de territoires. Prends les valeurs `zone`, `country`, `subdivision` ou `infrasubdivision`.
 * @param null|string $pays    Code ISO 3166-1 alpha2 du pays si le type est `subdivision` ou `infrasubdivision` sinon une chaine vide.
 * @param null|array  $options Tableau des options de dépeuplement:
 *                             - `force` : si `true` force le vidage même si la meta n'est pas présente (cas d'erreur
 *                             sur timeout par exemple). La valeur par défaut est `false`.
 *
 * @return array Tableau retour de la fonction permettant de connaitre le résultat du traitement (utile pour l'affichage
 *               du message dans le formulaire de peuplement).
 */
function unite_peuplement_vider(string $type, ?string $pays = '', ?array $options = []) : array {
	// On initialise le retour à une erreur nok
	$retour = [
		'ok'   => false,
		'arg'  => false,
		'sha'  => false,
		'type' => $type,
		'pays' => $pays,
		'sync' => true
	];

	// Inutile de vider une table vide sauf si l'option de forçage est activée.
	include_spip('inc/unite_peuplement');
	if (
		unite_peuplement_est_chargee('territoires', $type, $pays)
		or !empty($options['force'])
	) {
		// Avant de vider la table on réserve la liste des id de territoire qui seront supprimés
		// de façon à vider ensuite les liens éventuels y compris ceux des logos.
		$from = 'spip_territoires';
		$where = [
			'type=' . sql_quote($type),
		];

		if (
			include_spip('inc/config')
			and lire_config("territoires/{$type}/populated_by_country", false)
		) {
			$where[] = 'iso_pays=' . sql_quote($pays);
		}
		if ($ids = sql_allfetsel('id_territoire', $from, $where)) {
			$ids = array_column($ids, 'id_territoire');

			$sql_ok = sql_delete($from, $where);
			if ($sql_ok !== false) {
				// Vider les codes extras si besoin
				$where[] = 'type_extra=' . sql_quote('code');
				$sql_ok = sql_delete('spip_territoires_extras', $where);

				if ($sql_ok !== false) {
					// Vider les liens éventuels avec les logos (on gère pas d'erreur)
					$where_logo = [
						sql_in('id_objet', $ids),
						'objet=' . sql_quote('territoire')
					];
					sql_delete('spip_documents_liens', $where_logo);

					// Vider les liens éventuels avec les autres objets (on gère pas d'erreur)
					$where_lien = [
						sql_in('id_territoire', $ids),
					];
					sql_delete('spip_territoires_liens', $where_lien);

					// Permettre à d'autres plugins de compléter le dépeuplement.
					// -- par exemple le plugin Contours supprime les liens vers les contours GIS (spip_liens_gis),
					//    mais pas les objets GIS.
					// -- suppression des extras info ou stat dans le plugin Jeux de données pour Territoires, etc.
					$flux = [
						'args' => [
							'type'           => $type,
							'pays'           => $pays,
							'ids_territoire' => $ids,
						],
						'data' => []
					];
					pipeline('post_depeupler_territoire', $flux);

					// Supprimer la meta propre au pays même si le vidage des liens est en erreur.
					include_spip('inc/config');
					$id_consigne = unite_peuplement_consigne_identifier('territoires', $type, $pays);
					effacer_config($id_consigne);
				}

				// Enregistrer le succès ou pas du déchargement de la table
				if ($sql_ok !== false) {
					// Succès complet
					$retour['ok'] = true;
					spip_log("Les territoires (Type '{$type}' - Pays '{$pays}') ont été vidés avec succès ainsi que les extras", 'territoires' . _LOG_DEBUG);
				} else {
					spip_log("Les territoires (Type '{$type}' - Pays '{$pays}') ont été vidés avec succès mais erreur lors du vidage des extras", 'territoires' . _LOG_ERREUR);
				}
			} else {
				spip_log("Erreur de vidage des territoires (Type '{$type}' - Pays '{$pays}')", 'territoires' . _LOG_ERREUR);
			}
		}
	} else {
		$retour['sha'] = true;
		spip_log("Aucun territoire (Type '{$type}' - Pays '{$pays}') à vider", 'territoires' . _LOG_AVERTISSEMENT);
	}

	return $retour;
}

/**
 * Teste si les codes alternatifs ou les caractéristiques additionnelles d’un type de territoire sont chargées en base.
 * La fonction lit la meta de chargement et non la table `spip_territoires_extras`.
 *
 * @api
 *
 * @uses unite_peuplement_consigne_identifier()
 *
 * @param string $type       Type de territoires. Prends les valeurs `zone`, `country`, `subdivision` ou `infrasubdivision`.
 * @param string $pays       Code ISO 3166-1 alpha2 du pays si le type est `subdivision` ou `infrasubdivision` sinon une chaine vide.
 * @param string $type_extra Type d'extra. Prends la valeur `code` uniquement pour Territoires mais d'autres valeurs possibles
 *                           via des plugins complémentaires.
 *
 * @return bool `true` si le territoire est chargé, `false` sinon.
 */
function unite_peuplement_extra_est_charge(string $type, string $pays, string $type_extra) : bool {
	// Initialisation de la liste
	$est_peuple = false;

	// Identification de la variable de consignation pour l'unite de peuplement (type, pays)
	include_spip('inc/unite_peuplement');
	$id_consigne = unite_peuplement_consigne_identifier('territoires', $type, $pays);
	if (
		include_spip('inc/config')
		and ($peuplement = lire_config($id_consigne, []))
		and in_array($type_extra, $peuplement['ext'])
	) {
		$est_peuple = true;
	}

	return $est_peuple;
}

/**
 * Extrait, pour les régions, les pays ou les subdivisions d'un pays, la liste des territoires ayant fait l'objet
 * d'une modification manuelle (descriptif) et la liste associations vers ses mêmes territoires.
 *
 * Les extras ne sont pas sauvegardés car il ne sont ni modifiables ni indexés par un id mais par un code invariant.
 * Si l'extraction fait suite à un chargement asynchrone, les territoires modifiés sont sauvegardés dans un cache.
 *
 * @internal
 *
 * @uses cache_ecrire()
 *
 * @param string      $type    Type de territoires. Prends les valeurs `zone`, `country`, `subdivision` ou `infrasubdivision`.
 * @param null|string $pays    Code ISO 3166-1 alpha2 du pays si le type est `subdivision` ou `infrasubdivision` sinon une chaine vide.
 * @param null|array  $options L'index `cacher_asynchrone` à true permet de mettre la collection récupérée dans un cache spécifique. Cette
 *                             option est utilisée dans les chargements en mode asynchrone.
 *
 * @return array Liste des territoires ayant été modifiés : seules les champs éditables sont consignés.
 */
function unite_peuplement_preserver_edition(string $type, ?string $pays = '', ?array $options = []) : array {
	// Initialisation de la table et de la condition de base sur le type et éventuellement le pays.
	$territoires = [];
	$from = 'spip_territoires';
	$where = [
		'type=' . sql_quote($type),
	];
	if (
		include_spip('inc/config')
		and lire_config("territoires/{$type}/populated_by_country", false)
	) {
		$where[] = 'iso_pays=' . sql_quote($pays);
	}

	// Extraction des liens vers les territoires du pays
	$where_lien = [
		sql_in_select('id_territoire', 'id_territoire', $from, $where)
	];
	$territoires['liens'] = sql_allfetsel('*', 'spip_territoires_liens', $where_lien);

	// Extraction des liens de logos vers les territoires du pays
	$where_logo = [
		'objet=' . sql_quote('territoire'),
		sql_in_select('id_objet', 'id_territoire', $from, $where)
	];
	$territoires['logos'] = sql_allfetsel('*', 'spip_documents_liens', $where_logo);

	// Extraction de la correspondance id-iso pour les liens et les logos
	$territoires['ids'] = [];
	$select = ['id_territoire', 'iso_territoire'];
	// -- les liens
	$where_ids = [
		sql_in('id_territoire', array_column($territoires['liens'], 'id_territoire'))
	];
	if ($ids = sql_allfetsel($select, $from, $where_ids)) {
		$territoires['ids'] = array_column($ids, 'id_territoire', 'iso_territoire');
	}
	// -- les logos
	$where_ids = [
		sql_in('id_territoire', array_column($territoires['logos'], 'id_objet'))
	];
	if ($ids = sql_allfetsel($select, $from, $where_ids)) {
		$ids = array_column($ids, 'id_territoire', 'iso_territoire');
		$territoires['ids'] = array_merge($territoires['ids'], $ids);
	}

	// Extraction des territoires éditées.
	// -- détermination de la liste des champs éditables.
	$territoires['editions'] = [];
	include_spip('base/objets');
	$description_table = lister_tables_objets_sql($from);
	// -- pour le select, les champs éditables sont complétés par le code ISO.
	$select = array_merge($description_table['champs_editables'], ['iso_territoire']);
	$where[] = 'edite=' . sql_quote('oui');
	if ($editions = sql_allfetsel($select, $from, $where)) {
		// -- indexer le tableau par le code iso de façon à simplifier la réintégration.
		$territoires['editions'] = array_column($editions, null, 'iso_territoire');
	}

	// Permettre à d'autres plugins de compléter la sauvegarde (principalement pour les liens).
	// -- par exemple le plugin Contours sauvegarde les liens vers les contours GIS (spip_liens_gis).
	$flux = [
		'args' => [
			'type' => $type,
			'pays' => $pays,
		],
		'data' => $territoires
	];
	$territoires = pipeline('post_preserver_territoire', $flux);

	// On peut demander à mettre en cache la collection.
	// Cela est utile dans le mode de chargement asynchrone avec itérations.
	if (!empty($options['cacher_asynchrone'])) {
		// Création d'un cache sécurisé des sauvegardes pour le relire si besoin de plusieurs itérations de chargement.
		include_spip('inc/ezcache_cache');
		$cache = [
			'sous_dossier' => 'territoires',
			'fonction'     => 'sauvegarde',
			'type'         => $type,
			'pays'         => $pays,
		];
		cache_ecrire('territoires', 'asynchrone', $cache, $territoires);
	}

	return $territoires;
}

/**
 * Compile, en fonction du mode de récupération (extra avec les données de territoire ou à part), les données
 * extra à insérer dans la table `spip_territoires_extras`.
 *
 * @internal
 *
 * @param string   $type          Type de territoires. Prends les valeurs `zone`, `country`, `subdivision` ou `infrasubdivision`.
 * @param string   $pays          Code ISO 3166-1 alpha2 du pays si le type est `subdivision` ou `infrasubdivision` sinon une chaine vide.
 * @param string   $mode          Mode de récupération des extras : `interne` ou `externe`.
 * @param string[] $types_extras  Liste des types d'extra à récupérer `code` pour Territoires mais d'autres valeurs sont possibles (plugins complémentaires).
 * @param array    $source        Source des extras qui peut être un territoire (si interne) ou les extras directement (si externe).
 * @param array    $configuration Configuration de l'extraction des données Nomenclatures pour le type concerné.
 * @param string[] &$meta_extras  Renvoie un tableau des types d'extra réellement récupérés
 *
 * @return array
 */
function unite_peuplement_compiler_extra(string $type, string $pays, string $mode, array $types_extras, array $source, array $configuration, array &$meta_extras) : array {
	$extras = [];

	foreach ($types_extras as $_type_extra) {
		// Vérifier qu'il existe des extras pour le type de territoire
		// -- on traite les deux modes d'extraction : interne à un objet territoire ou un bloc externe
		if (
			(
				($mode === 'interne')
				and !empty($configuration['champs']['extras'][$_type_extra])
			)
			or (
				($mode !== 'interne')
				and !empty($configuration['extras'][$_type_extra])
				and isset($source[$configuration['extras'][$_type_extra]['index']])
			)
		) {
			// On initialise les données communes de chaque extra
			$extra_defaut = [
				'type_extra' => $_type_extra,
				'type'       => $type,
				'iso_pays'   => $pays,
			];

			// Suivant le mode on enregistre chaque extra détecté:
			// - mode interne : chaque champ extra du territoire devient un objet extra de la table spip_territoires_extras
			// - mode externe : chaque élément du bloc idoine de la collection devient un objet extra
			$source_extras = ($mode === 'interne')
				? $configuration['champs']['extras'][$_type_extra]
				: $source[$configuration['extras'][$_type_extra]['index']];
			foreach ($source_extras as $_cle => $_valeur) {
				$extra = $extra_defaut;
				$extra['valeur'] = ($mode === 'interne')
					? $source[$_cle]
					: $_valeur[$configuration['extras'][$_type_extra]['champs']['valeur']];
				if (
					$extra['valeur']
					and ($extra['valeur'] !== '0000-00-00 00:00:00')
				) {
					$extra['iso_territoire'] = ($mode === 'interne')
						? $source['iso_territoire']
						: $_valeur[$configuration['extras'][$_type_extra]['cle_iso']];
					$extra['extra'] = ($mode === 'interne')
						? $_valeur
						: (substr($configuration['extras'][$_type_extra]['champs']['extra'], 0, 1) === '#'
							? ltrim($configuration['extras'][$_type_extra]['champs']['extra'], '#')
							: $_valeur[$configuration['extras'][$_type_extra]['champs']['extra']]);
					$extras[] = $extra;
				}
			}

			// Enregistrer le type d'extra pour la meta de consignation
			if (!in_array($_type_extra, $meta_extras)) {
				$meta_extras[] = $_type_extra;
			}
		}
	}

	return $extras;
}
