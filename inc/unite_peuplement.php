<?php
/**
 * API de gestion des unités de peuplement des territoires, appelable par différents plugins utilisateurs.
 *
 * @package SPIP\TERRITOIRES\API\UNITE_PEUPLEMENT
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// -----------------------------------------------------------------------
// --------------------------- API COMMUNES ------------------------------
// -----------------------------------------------------------------------

/**
 * Vérifie si le serveur configuré pour récupérer les informations sur les territoires est compatible avec le plugin
 * utilisateur passé en argument.
 *
 * @api
 *
 * @uses territoires_feed_initialiser_url_base()
 * @uses requeter_isocode()
 * @uses territoires_serveur_definir_compatibilite()
 *
 * @param string $plugin Préfixe du plugin appelant.
 *
 * @return bool `true` si le serveur est compatible avec la version du plugin appelant ou `false` sinon.
 */
function unite_peuplement_serveur_est_compatible(string $plugin) : bool {
	// Initialisation de la sortie et de l'index des collections du serveur.
	$est_compatible = false;
	static $index_collections = [];

	// Identifier l'url de base pour le plugin appelant
	include_spip('territoires/territoires');
	$url_base = territoires_feed_initialiser_url_base($plugin);

	// Acquérir l'index des collections du serveur
	if (!$index_collections) {
		// Appel à l'API REST de Nomenclatures
		$requeter = charger_fonction('requeter_isocode', 'inc');
		$reponse = $requeter($url_base);
		if ((int) ($reponse['erreur']['status']) === 200) {
			$index_collections = $reponse['donnees'];
		}
	}

	// Vérifier la compatibilité avec le plugin Nomenclatures qui fournit les données.
	$version_serveur = $index_collections['isocode']['fournisseur']['version'] ?? '';
	if ($version_serveur) {
		// on collecte les bornes min et max de compatibilité
		$bornes = territoires_serveur_definir_compatibilite($plugin);
		$vmin = $bornes['vmin'] ?? '0.0.0';
		$vmax = $bornes['vmax'] ?? '999.999.999';

		// vérifier si la version du serveur est comprise entre les bornes admissibles
		include_spip('inc/utils');
		if (
			spip_version_compare($vmin, $version_serveur, '<=')
			and spip_version_compare($vmax, $version_serveur, '>=')
		) {
			$est_compatible = true;
		}
	}

	return $est_compatible;
}

/**
 * Teste, pour un plugin utilisateur donné, si une unité de peuplement est chargée en base.
 * Cette fonction lit la consignation en meta et non la table spip_territoires.
 *
 * Est utilisable pour la nomenclature des territoires et les contours.
 *
 * @api
 *
 * @uses unite_peuplement_consigne_identifier()
 *
 * @param string      $plugin  Préfixe du plugin appelant.
 * @param string      $type    Type de territoires. Prends les valeurs `zone`, `country`, `subdivision` ou `infrasubdivision`.
 * @param null|string $pays    Code ISO 3166-1 alpha2 du pays si le type est `subdivision` ou `infrasubdivision` sinon une chaine vide.
 * @param null|string $service Identifiant du feed Nomenclatures ayant permis le chargement. Est utilisé pour indexer
 *                             la consignation dans le cas des contours uniquement. Sinon vaut chaine vide.
 *
 * @return string
 */
function unite_peuplement_est_chargee(string $plugin, string $type, ?string $pays = '', ?string $service = '') : string {
	// Initialisation de la liste
	$existe = false;

	// Définir la variable de config
	$id_consigne = unite_peuplement_consigne_identifier($plugin, $type, $pays, $service);
	if (
		include_spip('inc/config')
		and lire_config($id_consigne, [])
	) {
		$existe = true;
	}

	return $existe;
}

/**
 * Teste, pour un plugin utilisateur donné, si une unité de peuplement chargée est obsolète.
 * Cette fonction lit le sha de la consignation en meta et le compare au hash du feed correspondant sur le serveur
 * configuré.
 *
 * @api
 *
 * @uses unite_peuplement_consigne_identifier()
 * @uses unite_peuplement_informer_feeds()
 *
 * @param string      $plugin  Préfixe du plugin appelant.
 * @param string      $type    Type de territoires. Prends les valeurs `zone`, `country`, `subdivision` ou `infrasubdivision`.
 * @param null|string $pays    Code ISO 3166-1 alpha2 du pays si le type est `subdivision` ou `infrasubdivision` sinon une chaine vide.
 * @param null|string $service Identifiant du feed Nomenclatures ayant permis le chargement. Est utilisé pour indexer
 *                             la consignation dans le cas des contours uniquement. Sinon vaut chaine vide.
 * @param null|array  $feeds   Liste des feeds disponibles pour le type donné.
 *
 * @return bool `true` si le sha passé en argument est identique au sha stocké pour la table choisie, `false` sinon.
 */
function unite_peuplement_est_obsolete(string $plugin, string $type, ?string $pays = '', ?string $service = '', array $feeds = []) : bool {
	$est_obsolete = false;

	// On récupère le sha de l'unité de peuplement dans la metas si il existe (ie. la table a été chargée)
	include_spip('inc/config');
	$id_consigne = unite_peuplement_consigne_identifier($plugin, $type, $pays, $service);
	$sha_stocke = lire_config("{$id_consigne}/sha", []);

	// Récupérer le hash de l'unité de peuplement
	if ($feeds) {
		$hash = $feeds['hash'] ?? [];
	} else {
		$hash = unite_peuplement_informer_feeds($plugin, $type, $pays, $service, 'hash');
	}
	// -- si le feed est précisé on l'extrait seul
	if ($service) {
		$hash = [$service => $hash[$service]];
	}

	foreach ($hash as $_id_feed => $_shas) {
		if (
			empty($sha_stocke[$_id_feed])
			or (
				is_string($sha_stocke[$_id_feed])
				and is_string($_shas)
				and $sha_stocke[$_id_feed] !== $_shas
			)
		) {
			$est_obsolete = true;
			break;
		} elseif (
			is_array($sha_stocke[$_id_feed])
			and is_array($_shas)
		) {
			foreach ($_shas as $_pays => $_sha) {
				if ($sha_stocke[$_id_feed][$_pays] !== $_sha) {
					$est_obsolete = true;
					break;
				}
			}
			if ($est_obsolete) {
				break;
			}
		}
	}

	return $est_obsolete;
}

/**
 * Identifie la variable de consignation d'un peuplement.
 * Est utilisable pour la nomenclature des territoires et les contours.
 *
 * @api
 *
 * @param string      $plugin  Préfixe du plugin appelant.
 * @param string      $type    Type de territoires. Prends les valeurs `zone`, `country`, `subdivision` ou `infrasubdivision`.
 * @param null|string $pays    Code ISO 3166-1 alpha2 du pays si le type est `subdivision` ou `infrasubdivision` sinon une chaine vide.
 * @param null|string $service Identifiant du feed Nomenclatures ayant permis le chargement. Est utilisé pour indexer
 *                             la consignation dans le cas des contours uniquement. Sinon vaut chaine vide.
 *
 * @return string
 */
function unite_peuplement_consigne_identifier(string $plugin, string $type, ?string $pays = '', ?string $service = '') : string {
	// Initialisation avec la meta de base
	$id_consigne = "{$plugin}_peuplement_";

	// Structure de base pour les territoires
	$id_consigne .= $pays
		? "{$type}/{$pays}"
		: $type;

	// Complément pour les contours
	$id_consigne .= $service ? "/{$service}" : '';

	return $id_consigne;
}

/**
 * Acquiert les données de territoires disponibles dans Nomenclatures.
 * La fonction utilise l'API REST de Nomenclatures.
 *
 * @api
 *
 * @uses cache_est_valide()
 * @uses cache_lire()
 * @uses cache_ecrire()
 * @uses territoires_unite_peuplement_definir_collection()
 * @uses territoires_unite_peuplement_definir_filtre()
 * @uses territoires_feed_initialiser_url_base()
 * @uses requeter_isocode()
 *
 * @param string      $plugin  Préfixe du plugin appelant.
 * @param string      $type    Type de territoires. Prends les valeurs `zone`, `country`, `subdivision` ou `infrasubdivision`.
 * @param null|string $pays    Code ISO 3166-1 alpha2 du pays si le type est `subdivision` ou `infrasubdivision` sinon une chaine vide.
 * @param null|string $service Identifiant du feed Nomenclatures ayant permis le chargement. Est utilisé pour indexer
 *                             la consignation dans le cas des contours uniquement. Sinon vaut chaine vide.
 * @param null|array  $options L'option `cacher_synchrone` à true permet d'utiliser un cache pour limiter les requêtes au serveur
 *                             et accélérer la fourniture des données.
 *                             L'option `cacher_asynchrone` à true permet de mettre la collection récupérée dans un cache temporaire
 *                             pour être utilisée lors des itérations en mode asynchrone dans une fonction idoine.
 *
 * @return array Liste des données de territoires telles que fournies par l'API REST
 */
function unite_peuplement_acquerir(string $plugin, string $type, ?string $pays = '', ?string $service = '', ?array $options = []) : array {
	// Initialiser les territoires à vide pour gérer une éventuelle erreur de type.
	$donnees = [];

	// Vérifier l'utilisation d'un cache synchrone.
	include_spip('inc/ezcache_cache');
	$cache = [];
	$utiliser_cache_synchrone = false;
	if (!empty($options['cacher_synchrone'])) {
		$utiliser_cache_synchrone = true;
		$cache = [
			'sous_dossier' => $plugin,
			'type'         => $type,
		];
		if ($pays) {
			$cache['pays'] = $pays;
		}
		if ($service) {
			$cache['service'] = $service;
		}
	}

	if (
		$utiliser_cache_synchrone
		and ($fichier_cache = cache_est_valide('territoires', 'synchrone', $cache))
	) {
		// Lecture des données du fichier cache valide
		$donnees = cache_lire('territoires', 'synchrone', $fichier_cache);
	} else {
		// Acquérir les données
		// -- déterminer la collection à utiliser.
		include_spip('territoires/territoires');
		$collection = territoires_unite_peuplement_definir_collection($plugin, $type);

		// -- collectionner les territoires avec l'API REST de Nomenclatures
		if ($collection) {
			// Déterminer les conditions à appliquer.
			$filtres = territoires_unite_peuplement_definir_filtre($plugin, $type, $pays, $service);

			// Identifier l'url de base pour le plugin appelant
			$url_base = territoires_feed_initialiser_url_base($plugin);

			// Appel à l'API REST de Nomenclatures
			$requeter = charger_fonction('requeter_isocode', 'inc');
			$reponse = $requeter($url_base, $collection, $filtres);
			if ((int) ($reponse['erreur']['status']) === 200) {
				$donnees = $reponse['donnees'];

				// Mise à jour du cache correspondant
				if ($utiliser_cache_synchrone) {
					cache_ecrire('territoires', 'synchrone', $cache, json_encode($donnees));
				}
			}
		}
	}

	// On peut demander à mettre en cache la collection pour gérer les appels asynchrones.
	if (!empty($options['cacher_asynchrone'])) {
		// Création d'un cache sécurisé de la collection pour le relire si besoin de plusieurs itérations de
		// chargement.
		$cache = [
			'sous_dossier' => $plugin,
			'fonction'     => 'collection',
			'type'         => $type,
		];
		if ($pays) {
			$cache['pays'] = $pays;
		}
		cache_ecrire('territoires', 'asynchrone', $cache, $donnees);
	}

	return $donnees;
}

/**
 * Liste les feeds fournissant des nomenclatures de territoires pour l'unité de peuplement requise.
 *
 * @api
 *
 * @uses territoires_feed_categorie_lister()
 * @uses territoires_feed_acquerir()
 * @uses territoires_feed_indexer_hash()
 *
 * @param string      $plugin      Préfixe du plugin appelant.
 * @param string      $type        Type de territoires. Prends les valeurs `zone`, `country`, `subdivision`, `protected_area` ou `infrasubdivision`.
 * @param null|string $pays        Code ISO 3166-1 alpha2 du pays si le type est `subdivision` ou `infrasubdivision` sinon une chaine vide.
 * @param null|string $service     Identifiant du feed Nomenclatures ayant permis le chargement. Est utilisé pour indexer
 * @param null|string $information Information précise à renvoyer seule ou tout si vide. Les valeurs possibles sont
 *                                 `feed`, `pays`, `hash` ou `credit`.
 *
 * @return array Liste des feeds.
 */
function unite_peuplement_informer_feeds(string $plugin, string $type, ?string $pays = '', ?string $service = '', ?string $information = '') : array {
	// Initialisation du retour
	$infos = [];

	// Récupération des feeds pour le plugin appelant
	include_spip('territoires/territoires');
	$categories = territoires_feed_categorie_lister($plugin);

	if ($categories) {
		$feeds = territoires_feed_acquerir($plugin, $categories);
		if ($service) {
			$feeds = isset($feeds[$service])
				? [$service => $feeds[$service]]
				: [];
		}

		// Lire l'index du hash pour le type de territoire
		$index_hash = territoires_feed_indexer_hash($plugin, $type);

		foreach ($feeds as $_id_feed => $_feed) {
			// On écarte les feeds qui ne concerne pas le type concerné
			if (
				!empty($_feed['tags']['type'])
				and ($_feed['tags']['type'] === $type)
			) {
				// Pour le tag pays on a 3 cas :
				// - le feed a un tag vide ou nul
				// - le feed a un tag représentant un seul code
				// - le feed a un tag représentant une liste de codes séparés par une virgule
				$feed_matche = false;
				$pays_feed = [];
				$hash_feed = [];
				if (empty($_feed['tags']['pays'])) {
					// Type zone et country
					$feeds[$_id_feed]['tags']['pays'] = '';
					$pays_feed = [];
					$hash_feed[$_id_feed] = $_feed['records'][$index_hash];
					if (!$pays) {
						$feed_matche = true;
					}
				} elseif ($type === 'infrasubdivision') {
					// Type infrasubdivision d'un pays donné ou pas
					$pays_feed = [$_feed['tags']['pays']];
					$hash_feed[$_id_feed] = $_feed['records'][$index_hash];
					if (
						!$pays
						or ($pays === $_feed['tags']['pays'])
					) {
						$feed_matche = true;
					}
				} elseif (!$pays) {
					// Type subdivision ou protected_area mais on ne filtre pas sur un pays (affichage)
					$pays_feed = explode(',', $_feed['tags']['pays']);
					$hash_feed[$_id_feed] = $_feed['records'][$index_hash];
					$feed_matche = true;
				} elseif (in_array($pays, explode(',', $_feed['tags']['pays']))) {
					// Type subdivision ou protected_area d'un pays donné
					$pays_feed = [$pays];
					$hash_feed[$_id_feed] = $_feed['records'][$index_hash][$pays] ?? $_feed['records'][$index_hash];
					$feed_matche = true;
				}

				// Si le feed matche avec l'unité de peuplement demandée, on l'insère dans la liste
				if ($feed_matche) {
					$infos['feed'][$_id_feed] = $feeds[$_id_feed];
					$infos['pays'] = array_unique(array_merge($infos['pays'] ?? [], $pays_feed));
					$infos['hash'] = array_merge($infos['hash'] ?? [], $hash_feed);
				}
			}
		}
	}

	if ($infos) {
		return ($information ? $infos[$information] : $infos);
	} else {
		return $infos;
	}
}

/**
 * Rétablit les liens d'un type d'objet avec les territoires peuplés ou repeuplés.
 *
 * @api
 *
 * @param string $type_lien   Type de liens à restaurer : `liens`, `logos` ou autre.
 * @param array  $sauvegardes Tableau des sauvegardes dans lequel puiser les liens existants
 * @param array  $ids_crees   Tableau des nouveaux id des territoires.
 * @param array  $config_lien Tableau de configuration de la table de liens concernée composé des index:
 *                            - `table`    : nom complet de la table spip
 *                            - `id_table` : nom du champ id du territoire
 *
 * @return void
 */
function unite_peuplement_retablir_liens(string $type_lien, array $sauvegardes, array $ids_crees, array $config_lien) : void {
	// On contruit la liste des enregistrements correspondant aux liens à rétablir.
	$liens = [];
	foreach ($sauvegardes[$type_lien] as $_lien) {
		// identifier le code iso à partir de l'ancien id
		$iso = array_search($_lien[$config_lien['id_table']], $sauvegardes['ids']);
		// en déduire le nouvel id du territoire et le mettre à jour dans le tableau
		// -- si l'iso n'est pas trouvé dans les nouveaux c'est que le territoire n'existe plus
		//    et qu'il ne faut pas rétablir les liens
		if (isset($ids_crees[$iso])) {
			$lien = $_lien;
			$lien[$config_lien['id_table']] = $ids_crees[$iso];
			$liens[] = $lien;
		}
	}

	// Insertion des liens en une seule requête
	if ($liens) {
		sql_insertq_multi($config_lien['table'], $liens);
	}
}
