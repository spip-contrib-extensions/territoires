<?php
/**
 * Ce fichier contient la fonction de requêtage des données du plugin Nomenclatures via son API REST.
 *
 * @package SPIP\TERRITOIRES\SERVICES
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_TERRITOIRES_COEFF_MAX_DISTANT')) {
	/**
	 * Coefficient multiplicateur de la constante SPIP `_INC_DISTANT_MAX_SIZE` permettant de calculer la taille max
	 * d'une réponse à `recuperer_url()` si aucune taille n'est précisée dans l'appel.
	 */
	define('_TERRITOIRES_COEFF_MAX_DISTANT', 8);
}

/**
 * Renvoie, à partir de l'url du service, le tableau des données demandées.
 * Le service utilise dans ce cas une chaine JSON qui est décodée pour fournir
 * le tableau de sortie. Le flux retourné par le service est systématiquement
 * transcodé dans le charset du site avant d'être décodé.
 *
 * @api
 *
 * @uses recuperer_url()
 *
 * @param string      $url_base   Endpoint du serveur
 * @param null|string $collection Nom de la collection ou vide si on veut récupérer l'index des collections du serveur.
 * @param null|array  $filtres    Tableau des filtres à appliquer à la collection
 * @param null|int    $taille_max Taille maximale du flux récupéré suite à la requête.
 *                                La valeur entière `0` désigne la taille par défaut.
 *
 * @throws JsonException
 *
 * @return array Tableau de la réponse.
 *               Si l'index `erreur['status']` indique le statut de la réponse.
 *               La valeur 200 permet de tester une requête réussie et dans ce cas
 *               l'index `page` du flux reçu est retourné après décodage JSON.
 */
function inc_requeter_isocode_dist(string $url_base, ?string $collection = '', ?array $filtres = [], ?int $taille_max = 0) : array {
	// Calcul de l'url complète
	// -- initialisation
	$url = $collection
		? "{$url_base}/{$collection}"
		: $url_base;
	// -- ajout des filtres
	foreach ($filtres as $_critere => $_valeur) {
		$url .= "&{$_critere}={$_valeur}";
	}

	// Options de la fonction de récupération du JSON
	include_spip('inc/distant');
	$options = [
		'transcoder' => true,
		'taille_max' => $taille_max ?: _INC_DISTANT_MAX_SIZE * _TERRITOIRES_COEFF_MAX_DISTANT,
	];

	// Acquisition du flux de données
	$flux = recuperer_url($url, $options);

	$reponse = [];
	if (
		!$flux
		or ($flux['status'] === 404)
		or empty($flux['page'])
	) {
		// Erreur serveur 501 (Not Implemented)
		$reponse['erreur'] = [
			'status'  => 501,
			'type'    => 'serveur_api_indisponible',
			'element' => 'serveur',
			'valeur'  => $url_base,
			'extra'   => ['url' => $url]
		];
		// Trace de log
		spip_log("API indiponible : {$url}", 'territoires' . _LOG_ERREUR);
	} else {
		try {
			// Transformation de la chaîne json reçue en tableau associatif
			$reponse = json_decode($flux['page'], true, 512, JSON_THROW_ON_ERROR);
		} catch (Exception $erreur) {
			// Erreur serveur 520 (Unknown Error)
			$reponse['erreur'] = [
				'status'  => 520,
				'type'    => 'serveur_reponse_json',
				'element' => 'serveur',
				'valeur'  => $url_base,
				'extra'   => [
					'url'         => $url,
					'erreur_json' => $erreur->getMessage()
				]
			];
			// Trace de log
			spip_log("Erreur d'analyse JSON pour l'URL `{$url}` : " . $reponse['erreur']['extra']['erreur_json'], 'territoires' . _LOG_ERREUR);
		}
	}

	return $reponse;
}
