<?php
/**
 * API spécifique des objets territoire.
 * Complète l'API standard de spip comme objet_créer(), objet_modifier(), objet_lire().
 *
 * @package SPIP\TERRITOIRES\API\TERRITOIRE
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_EXTRAIRE_MULTI')) {
	/**
	 * Restaure cette constante à partir de SPIP 4.2.
	 */
	define('_EXTRAIRE_MULTI', '@<multi>(.*?)</multi>@sS');
}

/**
 * Renvoie, une liste de descriptions de territoires éventuellement filtrée sur certains champs.
 *
 * @api
 *
 * @param null|array $filtres Tableau associatif `[champ] = valeur` de critères de filtres sur les descriptions de feed.
 *                            Les opérateurs possibles sont l'égalité, l'appartenance et leurs négation.
 *
 * @return array Tableau des descriptions des feeds indexé par l'id du feed ou tableau vide sinon.
 */
function territoire_repertorier(?array $filtres = []) : array {
	// On initialise le where avec le critère sur le plugin.
	$where = [];

	// On rajoute les filtres éventuels.
	if ($filtres) {
		foreach ($filtres as $_champ => $_critere) {
			$operateur = strpos($_critere, ',') === false ? '=' : 'IN';
			$valeur = $_critere;
			$not = '';
			if (substr($_critere, 0, 1) === '!') {
				$not = 'NOT';
				$valeur = ltrim($_critere, '!');
			}
			$where[] = $operateur === '='
				? $_champ . ($not ? '!' : '') . $operateur . sql_quote($valeur)
				: sql_in($_champ, explode(',', $valeur), $not);
		}
	}

	// On récupère tous les champs des feeds.
	// -- Tous le champs sauf la date de mise à jour et la liste des éventuels champs exclus.
	$from = 'spip_territoires';
	$description_table = sql_showtable($from, true);
	$select = array_diff(array_keys($description_table['field']), ['maj']);
	if ($territoires = sql_allfetsel($select, $from, $where)) {
		$territoires = array_column($territoires, null, 'iso_territoire');
	} else {
		$territoires = [];
	}

	return $territoires;
}

/**
 * Fournit l'ascendance géographique d'un territoire, par consultation dans la base de données.
 *
 * @api
 *
 * @param string      $iso_territoire Code ISO principal du territoire
 * @param null|string $iso_parent     Code ISO principal du parent direct du territoire concerné ou chaine vide sinon
 * @param null|string $ordre          Classement de la liste : `descendant`(défaut) ou `ascendant`.
 *
 * @return array Liste des territoires ascendants.
 */
function territoire_lire_ascendance(string $iso_territoire, ?string $iso_parent = null, ?string $ordre = 'descendant') : array {
	$ascendance = [];

	// Si on ne passe pas le parent correspondant au territoire pour lequel on cherche l'ascendance
	// alors on le cherche en base de données.
	// Le fait de passer le parent est uniquement une optimisation.
	if (null === $iso_parent) {
		$iso_parent = sql_getfetsel('iso_parent', 'spip_territoires', 'iso_territoire=' . sql_quote($iso_territoire));
	}

	// Si on a bien un parent on cherche si il y a un parent alternatif
	if ($iso_parent) {
		$iso_parent_alt = sql_getfetsel('iso_parent_alt', 'spip_territoires', 'iso_territoire=' . sql_quote($iso_territoire));

		while ($iso_parent) {
			$select = ['id_territoire', 'iso_territoire', 'iso_parent', 'iso_parent_alt', 'nom_usage', 'type', 'categorie'];
			$where = ['iso_territoire=' . sql_quote($iso_parent)];
			$territoire = sql_fetsel($select, 'spip_territoires', $where);
			if ($territoire) {
				$iso_parent_alt_parent = $territoire['iso_parent_alt'];
				$territoire['iso_parent_alt'] = $iso_parent_alt;
				$ascendance[] = $territoire;
				$iso_parent = $territoire['iso_parent'];
				$iso_parent_alt = $iso_parent_alt_parent;
			} else {
				// Le parent est d'un autre type qui n'est pas encore peuplé en base : on s'arrête.
				$iso_parent = '';
			}
		}
	}

	if ($ascendance and ($ordre === 'descendant')) {
		$ascendance = array_reverse($ascendance);
	}

	return $ascendance;
}

/**
 * Fournit, pour un territoire donné, tout ou partie des informations extras disponibles dans la table
 * spip_territoires_extras.
 * Cette fonction ne renvoie pas les champs du territoire lui-même (table spip_territoires).
 *
 * @api
 *
 * @param string      $iso_territoire Code ISO principal du territoire
 * @param null|string $information    Type d'information à renvoyer depuis la table spip_territoires_extras ou vide
 *                                    si on veut toutes les informations.
 *
 * @return array|string Information ou liste des informations.
 */
function territoire_lire_extras(string $iso_territoire, ?string $information = '') {
	// Initialisation de la sortie en fonction de la demande
	$informations = $information ? '' : [];

	// Quelque soit la demande, on récupère toutes les informations du territoire.
	$select = ['extra', 'valeur'];
	$where = ['iso_territoire=' . sql_quote($iso_territoire)];
	$extras = sql_allfetsel($select, 'spip_territoires_extras', $where);

	if ($extras) {
		$extras = array_column($extras, 'extra');
		$informations = $information
			? ($extras[$information] ?? '')
			: $extras;
	}

	return $informations;
}

/**
 * Initialise les champs de l'objet territoire à partir d'un élément reçu de l'API REST et en utilisant la configuration
 * du type concerné et complète avec le type, le nom d'usage, le descriptif et le parent.
 *
 * @internal
 *
 * @param array       $territoire Données d'un objet territoire
 * @param string      $type       Type de territoires. Prends les valeurs `zone`, `country`, `subdivision` ou `infrasubdivision`.
 * @param null|string $pays       Code ISO 3166-1 alpha2 du pays si le type est `subdivision` ou `infrasubdivision` sinon une chaine vide.
 *
 * @return array Données initialisées et complétées de l'objet territoire fourni
 */
function territoire_initialiser_enregistrement(array $territoire, string $type, ?string $pays = '') : array {
	// Traduire les champs de Isocode en champs pour Territoires
	include_spip('inc/config');
	$enregistrement = [];
	$champs = lire_config("territoires/{$type}/champs/base");
	foreach ($champs as $_champ_api => $_champ_territoire) {
		$enregistrement[$_champ_territoire] = $territoire[$_champ_api];
	}

	// Compléter systématiquement avec la date le type, le nom d'usage qui pour l'instant n'est pas fourni et le descriptif
	// TODO : pour l'instant Nomenclatures ne fournit pas de nom d'usage ni de descriptif.
	$enregistrement['date'] = date('Y-m-d H:i:s');
	$enregistrement['type'] = $type;
	$enregistrement['nom_usage'] = preg_replace('#\s+\([^)]*\)#', '', $enregistrement['iso_titre']);
	$enregistrement['descriptif'] = '';

	// Gestion des parentés inter-types : on remplit systématiquement le champ parent pour créer une hiérarchie complète
	// inter-type et ce que le type parent soit peuplé ou pas.
	// Cela revient :
	// -- à ajouter le pays d'appartenance des subdivisions de plus haut niveau
	if (
		in_array($type, ['subdivision', 'protected_area'])
		and !$enregistrement['iso_parent']
	) {
		// Le pays d'appartenance est toujours inclus dans le champ iso_pays
		$enregistrement['iso_parent'] = $enregistrement['iso_pays'];
	}
	// -- à ajouter la région d'appartenance des pays (pas de hiérarchie dans les pays) et dupliquer le code du pays
	//    dans le champ iso_pays pour faciliter les filtres.
	if ($type === 'country') {
		// La région d'appartenance est toujours inclus dans le champ code_num_region fourni par Nomenclatures
		$enregistrement['iso_parent'] = $territoire['code_num_region'];
		$enregistrement['iso_pays'] = $enregistrement['iso_territoire'];
	}

	return $enregistrement;
}

/**
 * Fusionne les traductions d'une balise `<multi>` avec celles d'une autre balise `<multi>`.
 *
 * L'une des balise est considérée comme prioritaire ce qui permet de régler le cas où la même
 * langue est présente dans les deux balises.
 * Si on ne trouve pas de balise `<multi>` dans l'un ou l'autre des paramètres, on considère que
 * le texte est tout même formaté de la façon suivante : texte0[langue1]texte1[langue2]texte2...
 *
 * @internal
 *
 * @param string $multi_prioritaire     Balise multi considérée comme prioritaire en cas de conflit sur une langue.
 * @param string $multi_non_prioritaire Balise multi considérée comme non prioritaire en cas de conflit sur une langue.
 *
 * @return string La chaine construite est toujours une balise `<multi>` complète ou une chaine vide sinon.
 */
function territoire_fusionner_traduction(string $multi_prioritaire, string $multi_non_prioritaire) : string {
	$multi_merge = '';

	// On extrait le contenu de la balise <multi> si elle existe.
	$multi_prioritaire = trim($multi_prioritaire);
	$multi_non_prioritaire = trim($multi_non_prioritaire);

	// Si les deux balises sont identiques ou si la balise non prioritaire est vide on sort directement
	// avec le multi prioritaire ce qui améliore les performances.
	if (
		($multi_prioritaire === $multi_non_prioritaire)
		or !$multi_non_prioritaire
	) {
		$multi_merge = $multi_prioritaire;
	} else {
		if (preg_match(_EXTRAIRE_MULTI, $multi_prioritaire, $match)) {
			$multi_prioritaire = trim($match[1]);
		}
		if (preg_match(_EXTRAIRE_MULTI, $multi_non_prioritaire, $match)) {
			$multi_non_prioritaire = trim($match[1]);
		}

		if ($multi_prioritaire) {
			if ($multi_non_prioritaire) {
				// On extrait les traductions sous forme de tableau langue=>traduction.
				if (
					include_spip('inc/filtres')
					and function_exists('extraire_trads')
				) {
					// Filtre extraire_trads avant spip 4.2
					$traductions_prioritaires = extraire_trads($multi_prioritaire);
					$traductions_non_prioritaires = extraire_trads($multi_non_prioritaire);
				} else {
					// Depuis spip 4.2 il faut passer par le collecteur Multis
					// -- on crée un objet collecteur multis
					include_spip('src/Texte/Collecteur/Multis');
					$collecteurMultis = new Spip\Texte\Collecteur\Multis();
					// -- on collecte les 2 multi en reconstituant la balise et on extrait les traductions
					//    car on est sur que le contenu est non vide.
					$multis = $collecteurMultis->collecter('<multi>' . $multi_prioritaire . '</multi>');
					$traductions_prioritaires = $multis[0]['trads'];
					$multis = $collecteurMultis->collecter('<multi>' . $multi_non_prioritaire . '</multi>');
					$traductions_non_prioritaires = $multis[0]['trads'];
				}

				// On complète les traductions prioritaires avec les traductions non prioritaires dont la langue n'est pas
				// présente dans les traductions prioritaires.
				foreach ($traductions_non_prioritaires as $_lang => $_traduction) {
					if (!array_key_exists($_lang, $traductions_prioritaires)) {
						$traductions_prioritaires[$_lang] = $_traduction;
					}
				}

				// On construit le contenu de la balise <multi> mergé à partir des traductions prioritaires mises à jour.
				// Les traductions vides sont ignorées.
				ksort($traductions_prioritaires);
				foreach ($traductions_prioritaires as $_lang => $_traduction) {
					if ($_traduction) {
						$multi_merge .= ($_lang ? '[' . $_lang . ']' : '') . trim($_traduction);
					}
				}
			} else {
				$multi_merge = $multi_prioritaire;
			}
		} else {
			$multi_merge = $multi_non_prioritaire;
		}

		// Si le contenu est non vide on l'insère dans une balise <multi>
		if ($multi_merge) {
			$multi_merge = '<multi>' . $multi_merge . '</multi>';
		}
	}

	return $multi_merge;
}
